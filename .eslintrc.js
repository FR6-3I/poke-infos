module.exports = {
    root: true,
    env: {
        browser: true,
        es2022: true,
        node: true,
    },
        extends: [
            'eslint:recommended',
            'plugin:react/recommended',
            'plugin:react-hooks/recommended',
            'plugin:jsx-a11y/strict',
            'next/core-web-vitals',
            'standard',
            'plugin:@typescript-eslint/recommended',
            'plugin:@typescript-eslint/recommended-requiring-type-checking',
            'plugin:prettier/recommended',
        ],
    plugins: ['@typescript-eslint', 'prettier'],
    parser: '@typescript-eslint/parser',
    parserOptions: {
        ecmaFeatures: {
              jsx: true
        },
        ecmaVersion: 12,
        sourceType: 'module',
        project: './tsconfig.json',
    },
    overrides: [
        {
            extends: [
                'plugin:jest/all',
                'plugin:jest-formatting/strict',
                'plugin:jest-dom/recommended',
                'plugin:testing-library/react',
            ],
            files: ['tests/**'],
        },
    ],
}
