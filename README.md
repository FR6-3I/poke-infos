# Poké-Infos

## Description

&emsp;&emsp;&emsp;Poké-Infos est une sorte de Pokédex basique en ligne. Il s'appuie sur deux API, l'une pour les données ([pokeapi.co](https://pokeapi.co)) et l'autre, pour les images ([pokemon.com](https://www.pokemon.com/fr)). L'application est une PWA ([Progressive Web App](https://fr.wikipedia.org/wiki/Progressive_web_app)), donc adaptable selon l'écran (responsive). Elle  est disponible en trois langues (français, anglais et allemand) et utilise trois thèmes (classique, pokéball et ectoplasma). Certains Pokémon n'ont pas d'image, c'est normal cela provient de l'API ne les fournissant pas.

&emsp;&emsp;&emsp;Ce projet utilise les technologies principales suivantes :

- Typescript
- Next.js
- Preact
- Material-UI
- Jest
- Testing Library

## Télémétrie

&emsp;&emsp;&emsp;Si vous voulez désactiver la télémétrie (Next.js) de manière globale sur la machine, veuillez saisir la commande suivante:

```shell
# npm
npx next telemetry disable

# pnpm
pnpx next telemetry disable

# yarn
yarn next telemetry disable
```

## Mise en place

&emsp;&emsp;&emsp;Tout d'abord, si le but est uniquement d'avoir un aperçu, d'essayer l'application. Il est possible de le faire directement à [cette adresse](https://poke-infos.francis-thery.dev). Pour une mise en ligne, consulter d'abord la partie Sécurité.

Télécharger l'application :

```shell
git clone https://gitlab.com/FR6-3I/poke-infos.git

cd poke-infos
```

Installer les dépendances du projet :

```shell
# npm
npm i

# pnpm
pnpm i

# yarn
yarn
```

Créer un fichier *.env.local* et y instancier les variables selon le schéma de *.env* si nécessaire.

Lancer l'application en mode production :

```shell
# npm
npm run build && npm run start

# pnpm
pnpm build && pnpm start

# yarn
yarn build && yarn start
```

## Sécurité

&emsp;&emsp;&emsp;Pour une mise en ligne, il est nécessaire de faire quelques manipulations :

- Paramétrer [X-Frame-Options](https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/X-Frame-Options).
- Paramétrer [X-Content-Type-Options](https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/X-Content-Type-Options).
- Rediriger les pages HTTP vers HTTPS.

&emsp;&emsp;&emsp;Après ces changements, OWASP ZAP ne devrait détecter que deux alertes de risque informationnel :
- La première correspond aux informations de construction (build) de Next.js et de la façon dont le code est transformé à la construction. OWASP ZAP déclenche l'alerte à la lecture du mot 'query', 'select', et 'from'.
- La seconde correspond aussi à une fausse alerte générée par Next.js. Il détecte de possibles dates (timestamp) qui n'en sont pas.

## Rapports Lighthouse

&emsp;&emsp;&emsp;Pour générer les rapports, suivre la mise en place et ensuite dans une nouvelle fenêtre, taper les commandes suivantes :

```shell
# S'assurer d'être dans le dossier poke-infos
mkdir rapports_lighthouse

# npm
npm run lh:all

# pnpm
pnpm lh:all

# yarn
yarn lh:all
```

&emsp;&emsp;&emsp;Le mauvais score sur la page du Pokémon provient des nombreuses données inutiles fournies par l'API des données, mais nécessaires au bon fonctionnement. Certains messages d'avertissement sont affichés :

- *Does not use passive listeners to improve scrolling performance* découle directement de la façon dont est codé React.js et se répercute donc sur Preact.
- *Remove unused JavaScript* vient du paquet *pokemon* servant à traduire les noms des Pokémon, à connaître leur numéro et qui les stocke tous dans un JSON utilisé par les fonctions. Aussi de données possiblement en cache pour la PWA.
- *Properly size images* est dû à l'optimisation d'images de Next.js qui émet parfois une requête pour une image avec une taille un cran au-dessus, mais qui est directement annulée (NS_BINDING_ABORTED) et réémise avec la bonne taille.
