import nextJest from 'next/jest'
import { Config } from 'jest'

const createJestConfig = nextJest({
  dir: './'
})

const config: Config = {
  resetMocks: true,
  setupFilesAfterEnv: [
    'jest-extended/all',
    '@testing-library/jest-dom',
    './tests/jest.setup.ts'
  ],
  testEnvironment: 'jsdom',
  verbose: true
}

export default createJestConfig(config)
