module.exports = {
  siteUrl: process.env.NEXT_PUBLIC_ADDRESS,
  changefreq: false,
  autoLastmod: false,
  generateRobotsTxt: true,
  exclude: [
    '/server-sitemap/sitemap-en.xml',
    '/server-sitemap/sitemap-fr.xml',
    '/server-sitemap/sitemap-de.xml',
    '/list'
  ],
  robotsTxtOptions: {
    additionalSitemaps: [
      `${process.env.NEXT_PUBLIC_ADDRESS}/server-sitemap/sitemap-en.xml`,
      `${process.env.NEXT_PUBLIC_ADDRESS}/server-sitemap/sitemap-fr.xml`,
      `${process.env.NEXT_PUBLIC_ADDRESS}/server-sitemap/sitemap-de.xml`
    ]
  }
}
