import withPlugins from 'next-compose-plugins'
import withPWA from 'next-pwa'
import withBundleAnalyzer from '@next/bundle-analyzer'

withBundleAnalyzer({
  enabled: process.env.ANALYZE === 'true'
})

/** @type {import('next').NextConfig} */
const nextConfig = {
  poweredByHeader: false,
  i18n: {
    locales: ['en', 'fr', 'de'],
    defaultLocale: 'en'
  },
  images: {
    domains: [process.env.API_IMAGE_DOMAIN]
  },
  transpilePackages: [
    'ky',
    'ky-universal'
  ]
}

const config = withPlugins([
  withBundleAnalyzer,
  [withPWA, {
    pwa: {
      dest: 'public'
    }
  }],
  nextConfig
])

export default config
