import { useRecoilValue } from 'recoil'
import { Box, Link } from '@mui/material'

import { langAtom } from '@/recoil/atoms'
import { links } from '@/utils/constants'

export const Footer = (): JSX.Element => {
    const lang = useRecoilValue(langAtom)

    return (
        <Box
            component='footer'
            sx={{
                marginTop: 'auto',
                padding: '3vh 0px',
                backgroundColor: 'primary.main',
            }}
        >
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'center',
                    color: 'text.primary',
                }}
            >
                <Link
                    href={links.PROFILE_GITLAB}
                    target='_blank'
                    rel='noreferrer'
                    sx={{
                        color: 'text.primary',
                        ':visited': {
                            color: 'text.primary',
                        },
                    }}
                >
                    {lang.footer}
                </Link>
            </Box>
        </Box>
    )
}
