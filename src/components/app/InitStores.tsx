import { useEffect } from 'react'

import { useThemeStore } from '@/stores/theme'
import { useLangStore } from '@/stores/lang'

export const InitStores = (): JSX.Element => {
    const { initTheme } = useThemeStore()
    const { initLang } = useLangStore()

    useEffect(() => {
        initTheme()
        initLang()
        /* eslint-disable-next-line react-hooks/exhaustive-deps */
    }, [])

    return <></>
}
