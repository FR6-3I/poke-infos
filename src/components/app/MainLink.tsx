import { useRecoilValue } from 'recoil'
import { Link } from '@mui/material'

import { langAtom } from '@/recoil/atoms'

export const MainLink = (): JSX.Element => {
    const lang = useRecoilValue(langAtom)

    return (
        <Link
            href='#main'
            sx={{
                zIndex: 10000,
                position: 'absolute',
                top: '-40px',
                left: '0px',
                padding: '8px',
                color: 'text.primary',
                backgroundColor: 'primary.dark',
                ':focus': {
                    top: '0px',
                },
            }}
        >
            {lang.skipLink}
        </Link>
    )
}
