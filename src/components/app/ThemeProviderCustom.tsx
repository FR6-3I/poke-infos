import { ReactNode } from 'react'
import { useRecoilValue } from 'recoil'
import { ThemeProvider } from '@mui/material'

import { themeValueAtom } from '@/recoil/atoms'

interface ThemeProviderCustomProps {
    children: ReactNode
}

export const ThemeProviderCustom = ({
    children,
}: ThemeProviderCustomProps): JSX.Element => {
    const themeValue = useRecoilValue(themeValueAtom)

    return <ThemeProvider theme={themeValue}>{children}</ThemeProvider>
}
