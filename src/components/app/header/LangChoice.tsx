import { useState } from 'react'
import { useRouter } from 'next/router'
import { useRecoilValue } from 'recoil'
import { Box, Button, Menu, MenuItem, Tooltip } from '@mui/material'
import { MdExpandMore, MdTranslate } from 'react-icons/md'

import { langAtom } from '@/recoil/atoms'
import { useLangStore } from '@/stores/lang'
import { getLocale, langName } from '@/utils/lang'
import { Locale } from '@/types/lang'

const LangChoice = (): JSX.Element => {
    const router = useRouter()
    const { setNewLang } = useLangStore()
    const lang = useRecoilValue(langAtom)

    const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null)
    const isOpen = Boolean(anchorEl)
    const locale = getLocale(router.locale)

    return (
        <>
            {/* Button */}
            <Tooltip title={lang.header.choixLangue}>
                <Button
                    id='langButton'
                    aria-controls={isOpen ? 'langMenu' : undefined}
                    aria-haspopup='true'
                    aria-expanded={isOpen ? 'true' : undefined}
                    onClick={(event) => setAnchorEl(event.currentTarget)}
                    sx={{
                        margin: '2px',
                        fontWeight: 'bold',
                        color: 'text.primary',
                        border: '2px solid transparent',
                        ':hover': {
                            backgroundColor: 'primary.light',
                        },
                        ':focus': {
                            border: '2px solid primary.light',
                            backgroundColor: 'primary.light',
                        },
                    }}
                >
                    <MdTranslate
                        size='1.5em'
                        role='presentation'
                    />

                    <Box
                        sx={{
                            display: {
                                xs: 'none',
                                sm: 'unset',
                            },
                            marginLeft: '5px',
                        }}
                    >
                        {langName[locale]}
                    </Box>

                    <MdExpandMore
                        size='1.5em'
                        role='presentation'
                    />
                </Button>
            </Tooltip>

            {/* Language list */}
            <Menu
                id='themeMenu'
                anchorEl={anchorEl}
                open={isOpen}
                onClose={() => setAnchorEl(null)}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
            >
                {Locale.options.map((loc) => (
                    <MenuItem
                        key={loc}
                        onClick={() => setNewLang(loc)}
                        selected={loc === locale}
                        aria-label={langName[loc]}
                        sx={{
                            color: 'text.primary',
                            ':hover': {
                                backgroundColor: 'primary.light',
                            },
                            ':focus': {
                                border: '2px solid primary.light',
                                backgroundColor: 'primary.light',
                            },
                        }}
                    >
                        {langName[loc]}
                    </MenuItem>
                ))}
            </Menu>
        </>
    )
}

export default LangChoice
