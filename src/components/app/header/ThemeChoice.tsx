import { useState } from 'react'
import { useRecoilValue } from 'recoil'
import { Box, Button, Menu, MenuItem, Tooltip } from '@mui/material'
import { MdExpandMore, MdPalette } from 'react-icons/md'

import { useThemeStore } from '@/stores/theme'
import { langAtom, themeNameAtom } from '@/recoil/atoms'
import { themeNameLang } from '@/utils/theme'
import { ThemeName } from '@/types/theme'

const ThemeChoice = (): JSX.Element => {
    const { setTheme } = useThemeStore()
    const lang = useRecoilValue(langAtom)
    const themeName = useRecoilValue(themeNameAtom)

    const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null)
    const isOpen = Boolean(anchorEl)

    return (
        <>
            {/* Button */}
            <Tooltip title={lang.header.choixTheme}>
                <Button
                    id='themeButton'
                    aria-controls={isOpen ? 'themeMenu' : undefined}
                    aria-haspopup='true'
                    aria-expanded={isOpen ? 'true' : undefined}
                    onClick={(event) => setAnchorEl(event.currentTarget)}
                    sx={{
                        margin: '2px',
                        fontWeight: 'bold',
                        color: 'text.primary',
                        border: '2px solid transparent',
                        ':hover': {
                            backgroundColor: 'primary.light',
                        },
                        ':focus': {
                            border: '2px solid primary.light',
                            backgroundColor: 'primary.light',
                        },
                    }}
                >
                    <MdPalette
                        size='1.5em'
                        role='presentation'
                    />

                    <Box
                        sx={{
                            display: {
                                xs: 'none',
                                sm: 'unset',
                            },
                            marginLeft: '5px',
                        }}
                    >
                        {themeNameLang[themeName]}
                    </Box>

                    <MdExpandMore
                        size='1.5em'
                        role='presentation'
                    />
                </Button>
            </Tooltip>

            {/* Theme list */}
            <Menu
                id='themeMenu'
                anchorEl={anchorEl}
                open={isOpen}
                onClose={() => setAnchorEl(null)}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
            >
                {ThemeName.options.map((val) => (
                    <MenuItem
                        key={val}
                        onClick={() => setTheme(val)}
                        selected={themeName === val}
                        aria-label={themeNameLang[val]}
                        sx={{
                            color: 'text.primary',
                            ':hover': {
                                backgroundColor: 'primary.light',
                            },
                            ':focus': {
                                border: '2px solid primary.light',
                                backgroundColor: 'primary.light',
                            },
                        }}
                    >
                        {themeNameLang[val]}
                    </MenuItem>
                ))}
            </Menu>
        </>
    )
}

export default ThemeChoice
