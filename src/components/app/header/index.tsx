import NextLink from 'next/link'
import Image from 'next/image'
import { useRecoilValue } from 'recoil'
import {
    AppBar,
    Box,
    Button,
    IconButton,
    Link,
    SxProps,
    Toolbar,
    Typography,
    useMediaQuery,
    useTheme,
} from '@mui/material'
import { BsGrid3X3GapFill } from 'react-icons/bs'

import { langAtom } from '@/recoil/atoms'
import ThemeChoice from './ThemeChoice'
import LangChoice from './LangChoice'
import { urls } from '@/utils/constants'

const styleButton: SxProps = {
    marginLeft: {
        xs: '10px',
        sm: '20px',
        md: '30px',
        lg: '50px',
        xl: '60px',
    },
    fontWeight: 'bold',
    color: 'text.primary',
    border: '2px solid transparent',
    ':hover': {
        textDecoration: 'none',
        backgroundColor: 'primary.light',
    },
    ':focus': {
        border: '2px solid primary.light',
        backgroundColor: 'primary.light',
    },
}

export const Header = (): JSX.Element => {
    const lang = useRecoilValue(langAtom)
    const theme = useTheme()
    const xs = useMediaQuery(theme.breakpoints.down('xs'))

    return (
        <AppBar sx={{ position: 'static' }}>
            <Toolbar
                sx={{
                    paddingLeft: {
                        xs: '2vw',
                        md: '5vw',
                        lg: '10vw',
                        xl: '15vw',
                    },
                    paddingRight: {
                        xs: '0px',
                        md: '5vw',
                        lg: '10vw',
                        xl: '15vw',
                    },
                }}
            >
                {/* Button homepage */}
                <NextLink
                    href={urls.HOMEPAGE}
                    passHref
                    legacyBehavior
                >
                    <Button
                        component={Link}
                        aria-label={lang.header.accueil}
                        sx={{
                            margin: '10px',
                            padding: '0px',
                            color: 'text.primary',
                            border: '4px double transparent',
                            borderRadius: '15px',
                            ':hover': {
                                textDecoration: 'none',
                                backgroundColor: 'primary.light',
                            },
                            ':focus': {
                                border: '4px solid background.default',
                                backgroundColor: 'primary.light',
                            },
                        }}
                    >
                        <Box
                            sx={{
                                width: {
                                    xs: '50px',
                                    sm: '60px',
                                    md: '65px',
                                    lg: '70px',
                                },
                                height: {
                                    xs: '50px',
                                    sm: '60px',
                                    md: '65px',
                                    lg: '70px',
                                },
                            }}
                        >
                            <Image
                                src={'/images/pokedex.png'}
                                alt='Logo'
                                width={60}
                                height={60}
                                quality={100}
                                role='presentation'
                            />
                        </Box>

                        <Typography
                            variant='h5'
                            sx={{
                                display: {
                                    xs: 'none',
                                    sm: 'unset',
                                },
                                fontWeight: 'bold',
                            }}
                        >
                            Poké-Infos
                        </Typography>
                    </Button>
                </NextLink>

                {/* Button list */}
                {xs && (
                    <NextLink
                        href={urls.POKEMON_LIST}
                        passHref
                        legacyBehavior
                    >
                        <IconButton
                            component={Link}
                            aria-label={lang.header.liste}
                            sx={styleButton}
                        >
                            <BsGrid3X3GapFill role='presentation' />
                        </IconButton>
                    </NextLink>
                )}

                {!xs && (
                    <NextLink
                        href={urls.POKEMON_LIST}
                        passHref
                        legacyBehavior
                    >
                        <Button
                            component={Link}
                            aria-label={lang.header.liste}
                            sx={styleButton}
                        >
                            <BsGrid3X3GapFill
                                size='1.5em'
                                role='presentation'
                            />

                            <Box sx={{ marginLeft: '5px' }}>
                                {lang.header.liste}
                            </Box>
                        </Button>
                    </NextLink>
                )}

                {/* Choice of the language and the theme */}
                <Box sx={{ marginLeft: 'auto' }}>
                    <LangChoice />

                    <ThemeChoice />
                </Box>
            </Toolbar>
        </AppBar>
    )
}
