import { useRouter } from 'next/router'
import { useRecoilValue } from 'recoil'
import { Box, Button, SxProps } from '@mui/material'
import PokeScan from 'pokemon'

import { langAtom } from '@/recoil/atoms'
import { useEffect, useState } from 'react'

const styleBouton: SxProps = {
    mx: '2vw',
    fontWeight: 'bold',
    color: 'text.primary',
    bgcolor: 'primary.main',
    border: '2px solid transparent',
    '&:hover': {
        bgcolor: 'primary.light',
    },
    '&:focus': {
        bgcolor: 'primary.light',
        border: '2px solid',
        borderColor: 'primary.main',
    },
}

interface ButtonsProps {
    getPokemon: () => void
}

export const Buttons = ({ getPokemon }: ButtonsProps): JSX.Element => {
    const router = useRouter()
    const lang = useRecoilValue(langAtom)

    const [randomPokemon, setRandomPokemon] = useState(false)

    // Go to random Pokémon
    useEffect(() => {
        const goToRandomPokemon = async () => {
            const randomPokemonName = PokeScan.random()

            return router.push({
                pathname: '/[pokemon]',
                query: { pokemon: randomPokemonName },
            })
        }

        if (randomPokemon)
            goToRandomPokemon().catch(() => {
                /* Empty */
            })
    }, [randomPokemon, router])

    return (
        <Box
            sx={{
                display: 'flex',
                justifyContent: 'center',
            }}
        >
            <Button
                onClick={getPokemon}
                aria-label={lang.pages.index.pokemon}
                sx={styleBouton}
            >
                {lang.pages.index.pokemon}
            </Button>

            <Button
                onClick={() => setRandomPokemon(true)}
                aria-label={lang.pages.index.pokemonAleatoire}
                sx={styleBouton}
            >
                {lang.pages.index.pokemonAleatoire}
            </Button>
        </Box>
    )
}
