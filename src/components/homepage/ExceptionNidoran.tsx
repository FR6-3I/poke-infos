import { Dispatch } from 'react'
import { useRecoilValue } from 'recoil'
import {
    Button,
    Dialog,
    DialogActions,
    DialogTitle,
    SxProps,
} from '@mui/material'

import { langAtom } from '@/recoil/atoms'

const styleBoutonNidoran: SxProps = {
    color: 'text.primary',
    bgcolor: 'primary.main',
    border: '2px solid transparent',
    '&:hover': {
        bgcolor: 'primary.light',
    },
    '&:focus': {
        bgcolor: 'primary.light',
        border: '2px solid',
        borderColor: 'primary.main',
    },
}

interface ExceptionNidoranProps {
    nidoran: boolean
    setNidoran: Dispatch<boolean>
    setId: Dispatch<number>
}

export const ExceptionNidoran = ({
    nidoran,
    setNidoran,
    setId,
}: ExceptionNidoranProps): JSX.Element => {
    const lang = useRecoilValue(langAtom)

    return (
        <Dialog
            open={nidoran}
            onClose={() => setNidoran(false)}
            aria-labelledby='message'
            sx={{
                '& .MuiDialog-paper': {
                    p: '20px',
                    bgcolor: 'secondary.dark',
                },
            }}
        >
            {/* Message */}
            <DialogTitle
                id='message'
                sx={{
                    textAlign: 'center',
                    color: 'text.secondary',
                }}
            >
                {lang.pages.index.nidoranTitre}
            </DialogTitle>

            {/* Choices */}
            <DialogActions
                sx={{
                    display: 'flex',
                    justifyContent: 'space-evenly',
                }}
            >
                <Button
                    onClick={() => setId(32)}
                    aria-label={lang.pages.index.male}
                    sx={styleBoutonNidoran}
                >
                    {lang.pages.index.male}
                </Button>

                <Button
                    onClick={() => setId(29)}
                    aria-label={lang.pages.index.femelle}
                    sx={styleBoutonNidoran}
                >
                    {lang.pages.index.femelle}
                </Button>
            </DialogActions>
        </Dialog>
    )
}
