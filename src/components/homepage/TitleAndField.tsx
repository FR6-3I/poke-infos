import { Dispatch } from 'react'
import { useRecoilValue } from 'recoil'
import { Alert, TextField, Typography } from '@mui/material'
import { brown } from '@mui/material/colors'

import { langAtom } from '@/recoil/atoms'

interface TitleAndFieldProps {
    getPokemon: () => void
    alert: boolean
    valSearch: string
    setValSearch: Dispatch<string>
}

export const TitleAndField = ({
    getPokemon,
    alert,
    valSearch,
    setValSearch,
}: TitleAndFieldProps): JSX.Element => {
    const lang = useRecoilValue(langAtom)

    const handleName = ({ currentTarget }: { currentTarget: HTMLElement }) => {
        const target = currentTarget as HTMLInputElement
        const val =
            target.value.charAt(0).toUpperCase() +
            target.value.slice(1).toLowerCase()

        setValSearch(val)
    }

    /** If the user press 'Entrer', launch the search. */
    const verifySearch = ({ key }: { key: string }) => {
        if (key === 'Enter') getPokemon()
    }

    return (
        <>
            {/* Title */}
            <Typography
                component='h1'
                variant='h4'
                sx={{
                    mt: '5vh',
                    mx: {
                        xs: '2vw',
                        sm: '10vw',
                        md: '20vw',
                        lg: '30vw',
                    },
                    color: 'text.secondary',
                    textAlign: 'center',
                    fontWeight: 'bold',
                }}
            >
                {lang.pages.index.titre}
            </Typography>

            {/* Legend */}
            <Typography
                variant='caption'
                sx={{
                    mb: '5vh',
                    color: 'text.secondary',
                    textAlign: 'center',
                }}
            >
                {lang.pages.index.legende}
            </Typography>

            {/* Error message */}
            {alert && (
                <Alert
                    severity='error'
                    aria-label={lang.pages.index.alerte}
                    sx={{
                        display: 'flex',
                        mb: '2vh',
                        mx: {
                            xs: '25vw',
                            sm: '30vw',
                            md: '35vw',
                            lg: '40vw',
                        },
                        textAlign: 'center',
                        fontWeight: 'bold',
                        color: brown[900],
                        bgcolor: 'error.light',
                        '& .MuiAlert-icon': {
                            my: 'auto',
                            color: brown[900],
                        },
                        '& .MuiAlert-message': {
                            mx: 'auto',
                        },
                    }}
                >
                    {lang.pages.index.alerte}
                </Alert>
            )}

            {/* Field name or ID */}
            <TextField
                id='champ-nom'
                label={lang.pages.index.label}
                value={valSearch}
                onKeyDown={verifySearch}
                onChange={handleName}
                variant='filled'
                autoComplete='off'
                InputProps={{
                    sx: {
                        fontWeight: 'bold',
                        color: 'black',
                    },
                }}
                sx={{
                    mb: '4vh',
                    mx: {
                        xs: '25vw',
                        sm: '30vw',
                        md: '35vw',
                        lg: '40vw',
                    },
                    borderTopLeftRadius: '4px',
                    borderTopRightRadius: '4px',
                    '& label': {
                        color: 'text.secondary',
                    },
                    '& label.Mui-focused': {
                        color: 'text.secondary',
                    },
                }}
            />
        </>
    )
}
