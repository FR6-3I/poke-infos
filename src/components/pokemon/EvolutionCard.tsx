import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import Image from 'next/image'
import { useRecoilValue } from 'recoil'
import { Box, ButtonBase, Link, SxProps } from '@mui/material'
import PokeScan from 'pokemon'

import { langAtom } from '@/recoil/atoms'
import { getLocale } from '@/utils/lang'
import { pokemonImageLink } from '@/utils/constants'

const styleEvolution: SxProps = {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    width: {
        xs: '100px',
        sm: '110px',
        md: '120px',
        lg: '140px',
    },
    m: '5px',
    p: '10px 20px',
    borderRadius: '30px',
}

const styleImage: SxProps = {
    position: 'relative',
    width: {
        xs: '70px',
        sm: '80px',
        md: '100px',
        lg: '120px',
    },
    height: {
        xs: '70px',
        sm: '80px',
        md: '100px',
        lg: '120px',
    },
    mb: '5px',
    borderRadius: '50%',
    bgcolor: 'primary.light',
}

interface EvolutionCardProps {
    idPokemon: number
    idEvol: number
}

export const EvolutionCard = ({
    idPokemon,
    idEvol,
}: EvolutionCardProps): JSX.Element => {
    const router = useRouter()
    const lang = useRecoilValue(langAtom)
    const [idPokemonSearch, setIdPokemonSearch] = useState(0)

    const locale = getLocale(router.locale)

    // Redirect to the evolution clicked
    useEffect(() => {
        const getPokemon = async (): Promise<void> => {
            const nomAnglais = PokeScan.getName(idPokemonSearch)
            setIdPokemonSearch(0)
            await router.push({
                pathname: '/[pokemon]',
                query: { pokemon: nomAnglais },
            })
        }

        if (idPokemonSearch)
            getPokemon().catch(() => {
                /* Empty */
            })
    }, [idPokemonSearch, router])

    return (
        <>
            {/* Actual Pokémon */}
            {idEvol === idPokemon && (
                <Box
                    key={idEvol}
                    sx={{
                        ...styleEvolution,
                        bgcolor: 'primary.dark',
                    }}
                >
                    <Box sx={styleImage}>
                        <Image
                            src={pokemonImageLink(idEvol)}
                            alt={lang.pages.pokemon.image}
                            role='presentation'
                            quality={100}
                            fill
                        />
                    </Box>

                    <Box>{PokeScan.getName(idEvol, locale)}</Box>
                    <Box>{`(n°${idEvol})`}</Box>
                </Box>
            )}

            {/* Not actual Pokémon */}
            {idEvol !== idPokemon && (
                <ButtonBase
                    key={idEvol}
                    component={Link}
                    onClick={() => setIdPokemonSearch(idEvol)}
                    aria-label={`${PokeScan.getName(
                        idEvol,
                        locale,
                    )} (n°${idEvol})`}
                    sx={{
                        ...styleEvolution,
                        color: 'text.primary',
                        '&:hover': {
                            textDecoration: 'none',
                            bgcolor: 'primary.light',
                        },
                        '&:focus': {
                            bgcolor: 'primary.light',
                        },
                    }}
                >
                    <Box>
                        <Box sx={styleImage}>
                            <Image
                                src={pokemonImageLink(idEvol)}
                                alt={lang.pages.pokemon.image}
                                role='presentation'
                                quality={100}
                                fill
                            />
                        </Box>
                        <Box>{PokeScan.getName(idEvol, locale)}</Box>
                        <Box>{`(n°${idEvol})`}</Box>
                    </Box>
                </ButtonBase>
            )}
        </>
    )
}
