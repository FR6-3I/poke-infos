import { useRouter } from 'next/router'
import Image from 'next/image'
import { useRecoilValue } from 'recoil'
import { Box, Card, CardContent, Typography } from '@mui/material'
import PokeScan from 'pokemon'

import { langAtom } from '@/recoil/atoms'
import { Types } from './'
import { getLocale } from '@/utils/lang'
import { getWeight, getSize } from '@/utils/infosAPI'
import { pokemonImageLink } from '@/utils/constants'
import { Data } from '@/pages/[pokemon]'

interface PokemonCardProps {
    idPokemon: number
    data: Data
}

export const PokemonCard = ({
    idPokemon,
    data,
}: PokemonCardProps): JSX.Element => {
    const router = useRouter()
    const lang = useRecoilValue(langAtom)

    const locale = getLocale(router.locale)

    return (
        <Card
            elevation={5}
            sx={{
                width: {
                    xs: '50vw',
                    sm: '40vw',
                    md: '30vw',
                    lg: '20vw',
                },
                m: '20px',
                p: '20px',
                textAlign: 'center',
                color: 'text.primary',
            }}
        >
            <CardContent>
                <Typography
                    component='h1'
                    variant='h5'
                >
                    {`${PokeScan.getName(idPokemon, locale)} (n°${idPokemon})`}
                </Typography>

                {/* Image */}
                <Box
                    sx={{
                        position: 'relative',
                        width: '20vw',
                        height: '20vw',
                        m: '20px auto',
                        borderRadius: '50%',
                        color: 'text.primary',
                        bgcolor: 'primary.light',
                    }}
                >
                    <Image
                        src={pokemonImageLink(idPokemon)}
                        alt={lang.pages.pokemon.image}
                        quality={100}
                        fill
                    />
                </Box>

                {/* Size and weight */}
                <Box
                    sx={{
                        display: {
                            md: 'flex',
                        },
                        justifyContent: {
                            md: 'space-around',
                        },
                    }}
                >
                    <Box>{`${lang.pages.pokemon.taille}: ${getSize(
                        data.height,
                    )}`}</Box>
                    <Box>{`${lang.pages.pokemon.poids}: ${getWeight(
                        data.weight,
                    )}`}</Box>
                </Box>

                {/* Types */}
                <Box
                    sx={{
                        display: 'flex',
                        mt: '20px',
                        mb: '10px',
                    }}
                >
                    {data.types.length > 1
                        ? lang.pages.pokemon.types
                        : lang.pages.pokemon.type}
                </Box>

                <Types types={data.types} />
            </CardContent>
        </Card>
    )
}
