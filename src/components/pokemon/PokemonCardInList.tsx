import NextLink from 'next/link'
import Image from 'next/image'
import { useRecoilValue } from 'recoil'
import { Box, ButtonBase, Card, CardContent, Link } from '@mui/material'
import PokeScan from 'pokemon'

import { langAtom } from '@/recoil/atoms'
import { pokemonImageLink } from '@/utils/constants'

interface PokemonCardInListProps {
    name: string
    id: number
}

export const PokemonCardInList = ({
    name,
    id,
}: PokemonCardInListProps): JSX.Element => {
    const lang = useRecoilValue(langAtom)

    return (
        <NextLink
            href={`/${PokeScan.getName(id)}`}
            passHref
        >
            <ButtonBase
                component={Link}
                aria-label={`${name} (n°${id})`}
                sx={{
                    m: {
                        xs: '5px',
                        sm: '10px',
                    },
                    '&:hover': {
                        textDecoration: 'none',
                    },
                    '&:hover > div': {
                        bgcolor: 'primary.light',
                    },
                    '&:focus > div': {
                        bgcolor: 'primary.light',
                    },
                }}
            >
                <Card
                    elevation={5}
                    sx={{
                        color: 'text.primary',
                        width: {
                            xs: '100px',
                            md: '120px',
                            lg: '140px',
                        },
                    }}
                >
                    <CardContent
                        sx={{
                            display: 'flex',
                            alignItems: 'center',
                            flexDirection: 'column',
                        }}
                    >
                        {/* Name */}
                        <Box>{name}</Box>

                        {/* ID */}
                        <Box>{`(n°${id})`}</Box>

                        {/* Image */}
                        <Box
                            sx={{
                                position: 'relative',
                                width: {
                                    xs: '70px',
                                    md: '90px',
                                    lg: '110px',
                                },
                                height: {
                                    xs: '70px',
                                    md: '90px',
                                    lg: '110px',
                                },
                                mt: '15px',
                                borderRadius: '50%',
                                color: 'text.primary',
                                bgcolor: 'primary.light',
                            }}
                        >
                            <Image
                                src={pokemonImageLink(id)}
                                alt={lang.pages.pokemon.image}
                                role='presentation'
                                quality={100}
                                fill
                            />
                        </Box>
                    </CardContent>
                </Card>
            </ButtonBase>
        </NextLink>
    )
}
