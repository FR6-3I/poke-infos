import { useRecoilValue } from 'recoil'
import { Box, Card, CardContent, SxProps, Typography } from '@mui/material'
import { MdArrowDropDown } from 'react-icons/md'

import { langAtom } from '@/recoil/atoms'
import { EvolutionCard } from './'
import { POKEMONS_TOTAL } from '@/utils/constants'
import { IdEvolutions } from '@/pages/[pokemon]'

const styleFleche: SxProps = {
    m: '-20px',
    color: 'background.default',
}

interface PokemonEvolutionsProps {
    idPokemon: number
    idEvolutions: IdEvolutions
}

export const PokemonEvolutions = ({
    idPokemon,
    idEvolutions,
}: PokemonEvolutionsProps): JSX.Element => {
    const lang = useRecoilValue(langAtom)

    return (
        <Card
            elevation={5}
            sx={{
                minWidth: {
                    xs: '60vw',
                    sm: '30vw',
                    md: '20vw',
                    lg: '15vw',
                },
                maxWidth: {
                    xs: '80vw',
                    md: '40vw',
                },
                m: {
                    sm: '0px 20px',
                },
                p: '20px',
                pb: {
                    sm: '0px',
                },
                textAlign: 'center',
            }}
        >
            <CardContent sx={{ p: '0px' }}>
                <Typography
                    component='h1'
                    variant='h5'
                    sx={{
                        mb: '20px',
                        color: 'text.primary',
                    }}
                >
                    {lang.pages.pokemon.evolutions}
                </Typography>

                <Box
                    sx={{
                        display: 'flex',
                        justifyContent: 'center',
                        flexDirection: 'column',
                        alignItems: 'center',
                        color: 'text.primary',
                    }}
                >
                    {Object.entries(idEvolutions).map(
                        ([nomStadeEvol, resStadeEvol]) => {
                            const idEvol = resStadeEvol as number
                            if (idEvol > POKEMONS_TOTAL) return null

                            // Affiche directement le Pokémon et une flèche si c'est le premier stade d'évolution
                            if (nomStadeEvol === 'premier') {
                                return (
                                    <>
                                        <EvolutionCard
                                            idPokemon={idPokemon}
                                            idEvol={idEvol}
                                        />
                                        <Box sx={styleFleche}>
                                            <MdArrowDropDown
                                                size='5em'
                                                role='presentation'
                                            />
                                        </Box>
                                    </>
                                )
                            }

                            // Affiche les évolutions de même niveau s'il y en a plusieurs au même stade ou juste une
                            return (
                                <Box key={nomStadeEvol}>
                                    <Box
                                        sx={{
                                            display: 'flex',
                                            flexWrap: 'wrap',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                        }}
                                    >
                                        {Object.entries(idEvol).map(
                                            ([nomStadeEvol, resStadeEvol]) => {
                                                const idEvol =
                                                    resStadeEvol as number
                                                if (idEvol > POKEMONS_TOTAL)
                                                    return null

                                                return (
                                                    <EvolutionCard
                                                        key={nomStadeEvol}
                                                        idPokemon={idPokemon}
                                                        idEvol={idEvol}
                                                    />
                                                )
                                            },
                                        )}
                                    </Box>

                                    {/* Affiche une flèche s'il reste des stades d'évolution à afficher */}
                                    {Object.values(idEvolutions)[
                                        Object.keys(idEvolutions).length - 1
                                    ] !== resStadeEvol &&
                                        // Vérifie si au moins un Pokémon ne fait pas partie de la huitième génération
                                        Object.values(
                                            Object.values(idEvolutions)[
                                                Object.keys(idEvolutions)
                                                    .length - 1
                                            ] as IdEvolutions['second'],
                                        ).some(
                                            (val) => val <= POKEMONS_TOTAL,
                                        ) && (
                                            <Box
                                                sx={{
                                                    m: '-20px',
                                                    color: 'background.default',
                                                }}
                                            >
                                                <MdArrowDropDown
                                                    size='5em'
                                                    role='presentation'
                                                />
                                            </Box>
                                        )}
                                </Box>
                            )
                        },
                    )}
                </Box>
            </CardContent>
        </Card>
    )
}
