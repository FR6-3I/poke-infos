import { useRouter } from 'next/router'
import { Box } from '@mui/material'

import { Data } from '@/pages/[pokemon]'
import { getLocale } from '@/utils/lang'
import { getType } from '@/utils/infosAPI'

interface TypesProps {
    types: Data['types']
}

export const Types = ({ types }: TypesProps): JSX.Element => {
    const router = useRouter()

    const locale = getLocale(router.locale)

    /** Get color corresponding to the type. */
    const getBackgroundColor = (typeRef: string): string => {
        switch (typeRef) {
            case 'normal':
                return '#ada594'
            case 'fighting':
                return '#5f2311'
            case 'flying':
                return '#5d73d4'
            case 'poison':
                return '#6b246e'
            case 'ground':
                return '#ad8c33'
            case 'rock':
                return '#9e863d'
            case 'bug':
                return '#88960e'
            case 'ghost':
                return '#454593'
            case 'steel':
                return '#8e8e9f'
            case 'fire':
                return '#c72100'
            case 'water':
                return '#0c67c2'
            case 'grass':
                return '#389a02'
            case 'electric':
                return '#e79302'
            case 'psychic':
                return '#dc3165'
            case 'ice':
                return '#6dd3f5'
            case 'dragon':
                return '#4e3ba4'
            case 'dark':
                return '#3c2d23'
            case 'fairy':
                return '#e08ee0'
            default:
                return 'white'
        }
    }

    return (
        <Box
            sx={{
                display: {
                    xs: 'inline-block',
                    sm: 'flex',
                },
                justifyContent: {
                    sm: 'space-around',
                },
            }}
        >
            {types.map((el) => (
                <Box
                    key={el.type.name}
                    sx={{
                        width: '120px',
                        m: '5px',
                        py: '6px',
                        fontWeight: 'bold',
                        borderRadius: '15px',
                        boxShadow: 'inset 0px 0px 8px 0px black',
                        bgcolor: getBackgroundColor(el.type.name),
                    }}
                >
                    <Box
                        sx={{
                            p: '2px',
                            borderRadius: '5px',
                            color: 'text.primary',
                            textShadow: `
              black 1px 1px 2px,
              black 1px -1px 2px,
              black -1px 1px 2px,
              black -1px -1px 2px
            `,
                        }}
                    >
                        {getType(el.type.name, locale)}
                    </Box>
                </Box>
            ))}
        </Box>
    )
}
