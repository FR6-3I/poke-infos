import { createEnv } from '@t3-oss/env-nextjs'
import { z } from 'zod'

export const env = createEnv({
    client: {
        NEXT_PUBLIC_ADDRESS: z.string().url(),
        NEXT_PUBLIC_API_IMAGE: z.string().url(),
    },
    server: {
        API_IMAGE_DOMAIN: z.string(),
        PORT: z
            .string()
            .transform((val) => parseInt(val, 10))
            .pipe(z.number().positive()),
    },
    runtimeEnv: {
        API_IMAGE_DOMAIN: process.env.API_IMAGE_DOMAIN,
        PORT: process.env.PORT,
        NEXT_PUBLIC_ADDRESS: process.env.NEXT_PUBLIC_ADDRESS,
        NEXT_PUBLIC_API_IMAGE: process.env.NEXT_PUBLIC_API_IMAGE,
    },
})
