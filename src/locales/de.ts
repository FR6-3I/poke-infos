import en from './en'

const de: typeof en = {
    skipLink: 'Zum Hauptinhalt springen',

    header: {
        accueil: 'Startseite',
        liste: 'Liste',
        choixLangue: 'Auswahl der Sprache',
        choixTheme: 'Auswahl des Themas',
        themes: {
            classique: 'Classic',
            pokeball: 'Pokéball',
            ectoplasma: 'Gengar',
        },
    },

    footer: 'Von mir selbst erstellt.',

    pages: {
        index: {
            titre: 'Willkommen auf Poké-Infos. Geben Sie den Namen oder die Nummer des Pokémon ein, nach dem Sie suchen.*',
            legende: '*Pokémon der achten Generation sind nicht enthalten.',
            alerte: 'Pokémon nirgends zu finden !',
            label: 'Name oder Nummer',
            pokemon: 'Suche',
            pokemonAleatoire: 'Zufällig',
            nidoranTitre: 'Nidoran, welches Geschlecht ?',
            male: 'Männlich',
            femelle: 'Weiblich',
        },

        pokemon: {
            precedent: 'Vorherige',
            suivant: 'Weiter',
            image: 'Bild des Pokemon',
            taille: 'Größe',
            poids: 'Gewicht',
            type: 'Typ:',
            types: 'Typen:',
            evolutions: 'Entwicklungen',
        },

        liste: {
            titre: 'Liste',
            plus: 'Mehr',
        },

        erreur: {
            titre: 'Fehler 404',
            message:
                'Es ist ein Fehler aufgetreten, bitte leiten Sie zur Startseite weiter, um sich unter die Pokémon zu wagen.',
        },
    },
}

export default de
