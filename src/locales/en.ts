const en = {
    skipLink: 'Skip to main content',

    header: {
        accueil: 'Homepage',
        liste: 'List',
        choixLangue: 'Choice of language',
        choixTheme: 'Choice of theme',
        themes: {
            classique: 'Classic',
            pokeball: 'Pokéball',
            ectoplasma: 'Gengar',
        },
    },

    footer: 'Created by myself.',

    pages: {
        index: {
            titre: 'Welcome on Poké-Infos. Enter the name or number of the Pokémon you want to search.*',
            legende: '*Eighth-generation Pokémon are not included.',
            alerte: 'Pokémon not found !',
            label: 'Name or number',
            pokemon: 'Search',
            pokemonAleatoire: 'Random',
            nidoranTitre: 'Nidoran of which sex ?',
            male: 'Male',
            femelle: 'Female',
        },

        pokemon: {
            precedent: 'Previous',
            suivant: 'Next',
            image: 'Image of the Pokémon',
            taille: 'Height',
            poids: 'Weight',
            type: 'Type:',
            types: 'Types:',
            evolutions: 'Evolutions',
        },

        liste: {
            titre: 'List',
            plus: 'More',
        },

        erreur: {
            titre: 'Error 404',
            message:
                'An error has occurred, please redirect to the home page to venture out among Pokémon.',
        },
    },
}

export default en
