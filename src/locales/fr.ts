import en from './en'

const fr: typeof en = {
    skipLink: 'Passer au contenu principal',

    header: {
        accueil: 'Accueil',
        liste: 'Liste',
        choixLangue: 'Choix de la langue',
        choixTheme: 'Choix du thème',
        themes: {
            classique: 'Classique',
            pokeball: 'Pokéball',
            ectoplasma: 'Ectoplasma',
        },
    },

    footer: 'Créé par moi-même.',

    pages: {
        index: {
            titre: 'Bienvenue sur Poké-Infos. Entrez le nom ou le numéro du Pokémon à rechercher (sensible aux accents).*',
            legende: '*Les Pokémon de huitième génération ne sont pas inclus.',
            alerte: 'Pokémon introuvable !',
            label: 'Nom ou numéro',
            pokemon: 'Rechercher',
            pokemonAleatoire: 'Aléatoire',
            nidoranTitre: 'Nidoran de quel sexe ?',
            male: 'Mâle',
            femelle: 'Femelle',
        },

        pokemon: {
            precedent: 'Précédent',
            suivant: 'Suivant',
            image: 'Image du Pokémon',
            taille: 'Taille',
            poids: 'Poids',
            type: 'Type:',
            types: 'Types:',
            evolutions: 'Évolutions',
        },

        liste: {
            titre: 'Liste',
            plus: 'Plus',
        },

        erreur: {
            titre: 'Erreur 404',
            message:
                "Une erreur est survenue, veuillez vous rediriger vers l'accueil afin de vous aventurer parmi les Pokémon.",
        },
    },
}

export default fr
