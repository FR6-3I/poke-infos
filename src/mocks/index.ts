import { setupServer } from 'msw/node'

import pokemon from './pokemon'

const requests = [...pokemon]

export const server = setupServer(...requests)
