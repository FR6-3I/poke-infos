import { rest } from 'msw'

const evolutions = rest.get(
    'https://pokeapi.co/api/v2/evolution-chain/:id',
    async (_req, res, ctx) => {
        // Golem
        return res(
            ctx.json({
                chain: {
                    species: {
                        url: 'https://pokeapi.co/api/v2/pokemon-species/74',
                    },
                    evolves_to: [
                        {
                            species: {
                                url: 'https://pokeapi.co/api/v2/pokemon-species/75',
                            },
                            evolves_to: [
                                {
                                    species: {
                                        url: 'https://pokeapi.co/api/v2/pokemon-species/76',
                                    },
                                },
                            ],
                        },
                    ],
                },
            }),
        )
    },
)

export default evolutions
