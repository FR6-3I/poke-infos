import evolutions from './evolutions'
import infos from './infos'
import species from './species'

const pokemon = [evolutions, infos, species]

export default pokemon
