import { rest } from 'msw'

const infos = rest.get(
    'https://pokeapi.co/api/v2/pokemon/:id',
    async (req, res, ctx) => {
        const { id } = req.params
        const idPokemon = parseInt(id as string)

        // Golem
        if (idPokemon === 76) {
            return res(
                ctx.json({
                    height: 14,
                    weight: 3_000,
                    types: [
                        { type: { name: 'rock' } },
                        { type: { name: 'ground' } },
                    ],
                }),
            )
        }

        // Melmetal
        if (idPokemon === 809) {
            return res(
                ctx.json({
                    height: 25,
                    weight: 8_000,
                    types: [{ type: { name: 'steel' } }],
                }),
            )
        }

        // Bulbasaur
        return res(
            ctx.json({
                height: 7,
                weight: 69,
                types: [
                    { type: { name: 'grass' } },
                    { type: { name: 'poison' } },
                ],
            }),
        )
    },
)

export default infos
