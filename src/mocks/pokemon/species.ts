import { rest } from 'msw'

const species = rest.get(
    'https://pokeapi.co/api/v2/pokemon-species/:id',
    async (_req, res, ctx) => {
        // Golem
        return res(
            ctx.json({
                evolution_chain: {
                    url: 'https://pokeapi.co/api/v2/evolution-chain/31',
                },
            }),
        )
    },
)

export default species
