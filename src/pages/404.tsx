import Head from 'next/head'
import Image from 'next/image'
import { useRecoilValue } from 'recoil'
import { Box, SxProps, Typography } from '@mui/material'

import { langAtom } from '@/recoil/atoms'
import { SITE_NAME } from '@/utils/constants'

interface Letter {
    sx: SxProps
    src: string
    alt: string
    width: number
}

const widthP = 508 / 800
const widthS = 626 / 800
const widthExclamation = 402 / 800

const styleO: SxProps = {
    margin: '1px',
    width: {
        xs: '60px',
        sm: '70px',
        md: '80px',
        lg: '100px',
    },
    height: 'fit-content',
}

const styleP: SxProps = {
    margin: '1px',
    width: {
        xs: `${widthP * 60}px`,
        sm: `${widthP * 70}px`,
        md: `${widthP * 80}px`,
        lg: `${widthP * 100}px`,
    },
    height: 'fit-content',
}

const styleS: SxProps = {
    margin: '1px',
    width: {
        xs: `${widthS * 60}px`,
        sm: `${widthS * 70}px`,
        md: `${widthS * 80}px`,
        lg: `${widthS * 100}px`,
    },
    height: 'fit-content',
}

const styleExclamation: SxProps = {
    marginLeft: {
        xs: '10px',
        sm: '15px',
        md: '20px',
    },
    width: {
        xs: `${widthExclamation * 60}px`,
        sm: `${widthExclamation * 70}px`,
        md: `${widthExclamation * 80}px`,
        lg: `${widthExclamation * 100}px`,
    },
    height: 'fit-content',
}

const Error = (): JSX.Element => {
    const lang = useRecoilValue(langAtom)

    const oops: Letter[] = [
        {
            sx: styleO,
            src: '/images/zarbi_O.png',
            alt: 'O',
            width: 100,
        },
        {
            sx: styleO,
            src: '/images/zarbi_O.png',
            alt: 'O',
            width: 100,
        },
        {
            sx: styleP,
            src: '/images/zarbi_P.png',
            alt: 'P',
            width: widthP * 100,
        },
        {
            sx: styleS,
            src: '/images/zarbi_S.png',
            alt: 'S',
            width: widthS * 100,
        },
        {
            sx: styleExclamation,
            src: '/images/zarbi_!.png',
            alt: '!',
            width: widthExclamation * 100,
        },
    ]

    return (
        <>
            <Head>
                <title>{`${lang.pages.erreur.titre} - ${SITE_NAME}`}</title>
            </Head>

            <>
                <Box
                    sx={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        flexDirection: 'column',
                        marginTop: '80px',
                    }}
                >
                    {/* 'OOPS !' in Zarbi */}
                    <Box
                        sx={{
                            display: 'flex',
                            marginBottom: '50px',
                        }}
                    >
                        {oops.map((el, i) => (
                            <Box
                                key={`${el.alt}${i}`}
                                sx={el.sx}
                            >
                                <Image
                                    src={el.src}
                                    alt={el.alt}
                                    width={el.width}
                                    height={100}
                                    quality={100}
                                    style={{
                                        objectPosition: '50% 50%',
                                        width: '100%',
                                        height: 'auto',
                                    }}
                                />
                            </Box>
                        ))}
                    </Box>

                    {/* Message */}
                    <Typography
                        component='h1'
                        variant='h4'
                        sx={{
                            margin: {
                                xs: '0px 10vw',
                                sm: '0px 15vw',
                                md: '0px 20vw',
                                lg: '0px 30vw',
                            },
                            textAlign: 'center',
                            color: 'text.secondary',
                        }}
                    >
                        {lang.pages.erreur.message}
                    </Typography>
                </Box>
            </>
        </>
    )
}

export default Error
