import { useEffect, useState } from 'react'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { useRecoilValue } from 'recoil'
import { Box, ButtonBase, SxProps } from '@mui/material'
import { MdKeyboardArrowLeft, MdKeyboardArrowRight } from 'react-icons/md'
import ky from 'ky-universal'
import PokeScan from 'pokemon'

import { langAtom } from '@/recoil/atoms'
import { PokemonCard, PokemonEvolutions } from '@/components/pokemon'
import { getLocale } from '@/utils/lang'
import setIdsEvol from '@/utils/setIdsEvol'
import { POKEMONS_TOTAL, SITE_NAME, urls } from '@/utils/constants'
import { Locale } from '@/types/lang'
import { env } from '@/env'

const styleButton: SxProps = {
    display: 'flex',
    flexDirection: 'column',
    alignSelf: {
        xs: 'self-start',
        md: 'auto',
    },
    width: '80px',
    m: {
        xs: '5px',
        md: '20px',
    },
    mt: {
        xs: '25vh',
        sm: '200px',
    },
    p: '10px',
    color: 'text.secondary',
    borderRadius: '10px',
    '&:hover': {
        bgcolor: 'primary.main',
    },
    '&:focus': {
        bgcolor: 'primary.main',
    },
    '& > svg': {
        m: '-5px',
        mt: '-10px',
        fontSize: '4em',
    },
}

const styleHide: SxProps = {
    display: {
        xs: 'none',
        sm: 'block',
    },
}

export interface Data {
    height: number
    weight: number
    types: [{ type: { name: string } }]
}

interface DataSpecies {
    evolution_chain: { url: string }
}

export interface Evolutions {
    chain: {
        species: { url: string }
        evolves_to: {
            species: { url: string }
            evolves_to: { species: { url: string } }[] | null[]
        }[]
    }
}

export interface IdEvolutions {
    premier: number
    second: {
        premier2: number
        second2?: number
        troisieme2?: number
        quatrieme2?: number
        cinquieme2?: number
        sixieme2?: number
        septieme2?: number
        huitieme2?: number
    }
    troisieme?: {
        premier3: number
        second3?: number
    }
}

const Pokemon = (): JSX.Element => {
    const router = useRouter()
    const lang = useRecoilValue(langAtom)

    const [idPokemon, setIdPokemon] = useState(0)
    const [idNewPokemon, setIdNewPokemon] = useState<number>()
    const [data, setData] = useState<Data>({
        height: 0,
        weight: 0,
        types: [{ type: { name: '' } }],
    })
    const [idEvolutions, setIdEvolutions] = useState<IdEvolutions>({
        premier: 0,
        second: { premier2: 0 },
    })

    const locale = getLocale(router.locale)
    const { pokemon } = router.query
    const namePokemon = pokemon as string
    const name = idPokemon ? PokeScan.getName(idPokemon, locale) : ''

    // Redirect to the new Pokémon to display
    useEffect(() => {
        const goToNewPokemon = async (): Promise<void> => {
            const pokemonNameInEnglish = PokeScan.getName(idNewPokemon ?? 0)

            setIdEvolutions({
                premier: 0,
                second: { premier2: 0 },
            })

            await router
                .push({
                    pathname: '/[pokemon]',
                    query: { pokemon: pokemonNameInEnglish },
                })
                .catch(() => {
                    /* Empty */
                })

            setIdNewPokemon(undefined)
        }

        if (idNewPokemon)
            goToNewPokemon().catch(() => {
                /* Empty */
            })
    }, [idNewPokemon, router])

    // Verify if the link is ok
    useEffect(() => {
        const exist = async (): Promise<void> => {
            try {
                setIdPokemon(PokeScan.getId(namePokemon))
            } catch (err) {
                await router.replace(urls.ERROR_404)
            }
        }

        if (namePokemon)
            exist().catch(() => {
                /* Empty */
            })
    }, [namePokemon, router])

    // Instanciate Pokémon data
    useEffect(() => {
        const data = async () => {
            const pokemonData = await ky
                .get(`https://pokeapi.co/api/v2/pokemon/${idPokemon}`)
                .json<Data>()
                .catch(() => {
                    /* Empty */
                })

            if (!pokemonData) return

            setData(pokemonData)

            const species = await ky
                .get(`https://pokeapi.co/api/v2/pokemon-species/${idPokemon}`)
                .json<DataSpecies>()
                .catch(() => {
                    /* Empty */
                })

            if (!species) return

            const evolutions = await ky
                .get(species.evolution_chain.url)
                .json<Evolutions>()
                .catch(() => {
                    /* Empty */
                })

            if (!evolutions) return

            setIdEvolutions(setIdsEvol(evolutions))
        }

        if (idPokemon)
            data().catch(() => {
                /* Empty */
            })
    }, [idPokemon])

    return (
        <>
            <Head>
                <title>
                    {name} - {SITE_NAME}
                </title>

                <link
                    rel='alternate'
                    hrefLang={Locale.enum.en}
                    href={`${env.NEXT_PUBLIC_ADDRESS}/${namePokemon}`}
                />

                <link
                    rel='alternate'
                    hrefLang={Locale.enum.fr}
                    href={`${env.NEXT_PUBLIC_ADDRESS}/${Locale.enum.fr}/${namePokemon}`}
                />

                <link
                    rel='alternate'
                    hrefLang={Locale.enum.de}
                    href={`${env.NEXT_PUBLIC_ADDRESS}/${Locale.enum.de}/${namePokemon}`}
                />
            </Head>

            <>
                {data.height !== 0 && (
                    <Box
                        sx={{
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}
                    >
                        {/* Previous Pokémon */}
                        {idPokemon > 1 && (
                            <ButtonBase
                                aria-label={lang.pages.pokemon.precedent}
                                onClick={() => setIdNewPokemon(idPokemon - 1)}
                                disabled={!!idNewPokemon}
                                sx={{
                                    ...styleButton,
                                    mr: {
                                        xs: '-30px',
                                        sm: '0px',
                                    },
                                }}
                            >
                                <MdKeyboardArrowLeft role='presentation' />

                                <Box sx={styleHide}>
                                    {lang.pages.pokemon.precedent}
                                </Box>
                            </ButtonBase>
                        )}

                        <Box
                            sx={{
                                display: 'flex',
                                justifyContent: 'center',
                                alignItems: 'center',
                                flexDirection: {
                                    xs: 'column',
                                    md: 'row',
                                },
                            }}
                        >
                            {/* Pokémon */}
                            <PokemonCard
                                idPokemon={idPokemon}
                                data={data}
                            />

                            {/* Evolutions */}
                            {idEvolutions.premier !== 0 &&
                                idEvolutions.premier <= POKEMONS_TOTAL &&
                                idEvolutions.second.premier2 <=
                                    POKEMONS_TOTAL && (
                                    <PokemonEvolutions
                                        idPokemon={idPokemon}
                                        idEvolutions={idEvolutions}
                                    />
                                )}
                        </Box>

                        {/* Next Pokémon */}
                        {idPokemon < POKEMONS_TOTAL && (
                            <ButtonBase
                                aria-label={lang.pages.pokemon.suivant}
                                onClick={() => setIdNewPokemon(idPokemon + 1)}
                                disabled={!!idNewPokemon}
                                sx={{
                                    ...styleButton,
                                    ml: {
                                        xs: '-30px',
                                        sm: '0px',
                                    },
                                }}
                            >
                                <MdKeyboardArrowRight role='presentation' />

                                <Box sx={styleHide}>
                                    {lang.pages.pokemon.suivant}
                                </Box>
                            </ButtonBase>
                        )}
                    </Box>
                )}
            </>
        </>
    )
}

export default Pokemon
