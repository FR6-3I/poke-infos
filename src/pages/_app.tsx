import { AppProps } from 'next/app'
import { RecoilEnv, RecoilRoot } from 'recoil'
import { Box, CssBaseline } from '@mui/material'
import { CacheProvider } from '@emotion/react'
import { EmotionCache } from '@emotion/cache'

import {
    Footer,
    Header,
    InitStores,
    MainLink,
    ThemeProviderCustom,
} from '@/components/app'
import createEmotionCache from '@/utils/createEmotionCache'

/** Client-side cache, shared for the whole session of the user in
 * the browser.
 */
const clientSideEmotionCache = createEmotionCache()

export interface MyAppProps extends AppProps {
    emotionCache?: EmotionCache
}

const MyApp = ({
    Component,
    emotionCache = clientSideEmotionCache,
    pageProps,
}: MyAppProps): JSX.Element => {
    RecoilEnv.RECOIL_DUPLICATE_ATOM_KEY_CHECKING_ENABLED = false

    return (
        <RecoilRoot>
            <InitStores />

            <CacheProvider value={emotionCache}>
                <ThemeProviderCustom>
                    <CssBaseline />

                    <Box
                        sx={{
                            display: 'flex',
                            flexDirection: 'column',
                            height: '100vh',
                        }}
                    >
                        {/* Link a11y to go to the main part */}
                        <MainLink />

                        {/* Header */}
                        <Header />

                        {/* Content */}
                        <Box
                            component='main'
                            id='main'
                            sx={{ margin: '5vh 0px' }}
                        >
                            <Component {...pageProps} />
                        </Box>

                        {/* Footer */}
                        <Footer />
                    </Box>
                </ThemeProviderCustom>
            </CacheProvider>
        </RecoilRoot>
    )
}

export default MyApp
