import { ComponentProps, ComponentType, ReactNode } from 'react'
import Document, {
    Html,
    Head,
    Main,
    NextScript,
    DocumentContext,
    DocumentProps,
} from 'next/document'
import { AppType } from 'next/app'
import createEmotionServer from '@emotion/server/create-instance'

import createEmotionCache from '@/utils/createEmotionCache'
import classic from '@/themes/classic'
import { MyAppProps } from './_app'

interface MyDocumentProps extends DocumentProps {
    emotionStyleTags: ReactNode
}

const MyDocument = ({ emotionStyleTags }: MyDocumentProps): JSX.Element => {
    return (
        <Html>
            <Head>
                <meta
                    name='description'
                    content='Information about the world of Pokémon'
                />
                <meta
                    name='theme-color'
                    content={classic.palette.primary.main}
                />

                <link
                    rel='preconnect'
                    href='https://pokeapi.co'
                />
                <link
                    rel='manifest'
                    href='/manifest.json'
                />
                <link
                    rel='icon'
                    href='/icons/icon-16x16.png'
                    type='image/png'
                    sizes='16x16'
                />
                <link
                    rel='icon'
                    href='/icons/icon-32x32.png'
                    type='image/png'
                    sizes='32x32'
                />
                <link
                    rel='icon'
                    href='/icons/icon-48x48.png'
                    type='image/png'
                    sizes='48x48'
                />
                <link
                    rel='apple-touch-icon'
                    href='/icons/icon-180x180.png'
                    sizes='180x180'
                />

                {emotionStyleTags}
            </Head>

            <body>
                <Main />
                <NextScript />
            </body>
        </Html>
    )
}

MyDocument.getInitialProps = async (ctx: DocumentContext) => {
    const originalRenderPage = ctx.renderPage
    const cache = createEmotionCache()
    /* eslint-disable-next-line @typescript-eslint/unbound-method */
    const { extractCriticalToChunks } = createEmotionServer(cache)

    ctx.renderPage = () =>
        originalRenderPage({
            enhanceApp: (
                App: ComponentType<ComponentProps<AppType> & MyAppProps>,
            ) =>
                function EnhanceApp(props) {
                    return (
                        <App
                            emotionCache={cache}
                            {...props}
                        />
                    )
                },
        })

    const initialProps = await Document.getInitialProps(ctx)
    // This is important. It prevents Emotion to render invalid HTML.
    // See https://github.com/mui/material-ui/issues/26561#issuecomment-855286153
    const emotionStyles = extractCriticalToChunks(initialProps.html)
    const emotionStyleTags = emotionStyles.styles.map((style) => (
        <style
            data-emotion={`${style.key} ${style.ids.join(' ')}`}
            key={style.key}
            /* eslint-disable-next-line react/no-danger */
            dangerouslySetInnerHTML={{ __html: style.css }}
        />
    ))

    return {
        ...initialProps,
        emotionStyleTags,
    }
}

export default MyDocument
