import { useEffect, useState } from 'react'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { Box } from '@mui/material'
import PokeScan from 'pokemon'

import { Buttons, ExceptionNidoran, TitleAndField } from '@/components/homepage'
import { getLocale } from '@/utils/lang'
import { SITE_NAME } from '@/utils/constants'
import { Locale } from '@/types/lang'
import { env } from '@/env'

const Homepage = (): JSX.Element => {
    const router = useRouter()

    const [valSearch, setValSearch] = useState('')
    const [id, setId] = useState(0)
    const [alert, setAlert] = useState(false)
    const [nidoran, setNidoran] = useState(false)

    const locale = getLocale(router.locale)

    /**
     * Instanciate the ID or display an error if the Pokémon is not found.
     * Cancel search if the field is empty.
     */
    const getPokemonId = () => {
        if (!valSearch) return

        if (valSearch === 'Nidoran') {
            setNidoran(true)

            return
        }

        try {
            if (Number(valSearch)) setId(Number(valSearch))
            else setId(PokeScan.getId(valSearch, locale))
        } catch (err) {
            setAlert(true)
        }
    }

    // Search the Pokémon with the given ID.
    // Display an error if the Pokémon is not found.
    useEffect(() => {
        const getPokemonName = async () => {
            try {
                const pokemonNameInEnglish = PokeScan.getName(id)

                await router.push({
                    pathname: '/[pokemon]',
                    query: { pokemon: pokemonNameInEnglish },
                })
            } catch (err) {
                setAlert(true)
            }
        }

        if (id)
            getPokemonName().catch(() => {
                /* Empty */
            })
    }, [id, router])

    return (
        <>
            <Head>
                <title>{SITE_NAME}</title>

                <link
                    rel='alternate'
                    hrefLang={Locale.enum.en}
                    href={`${env.NEXT_PUBLIC_ADDRESS}`}
                />

                <link
                    rel='alternate'
                    hrefLang={Locale.enum.fr}
                    href={`${env.NEXT_PUBLIC_ADDRESS}/${Locale.enum.fr}`}
                />

                <link
                    rel='alternate'
                    hrefLang={Locale.enum.fr}
                    href={`${env.NEXT_PUBLIC_ADDRESS}/${Locale.enum.fr}`}
                />
            </Head>

            <>
                <Box
                    sx={{
                        display: 'flex',
                        justifyContent: 'center',
                        flexDirection: 'column',
                    }}
                >
                    <TitleAndField
                        getPokemon={getPokemonId}
                        alert={alert}
                        valSearch={valSearch}
                        setValSearch={setValSearch}
                    />

                    <Buttons getPokemon={getPokemonId} />

                    <ExceptionNidoran
                        nidoran={nidoran}
                        setNidoran={setNidoran}
                        setId={setId}
                    />
                </Box>
            </>
        </>
    )
}

export default Homepage
