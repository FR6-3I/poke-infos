import { useEffect, useState } from 'react'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { useRecoilValue } from 'recoil'
import { Box, Button } from '@mui/material'
import PokeScan from 'pokemon'

import { langAtom } from '@/recoil/atoms'
import { PokemonCardInList } from '@/components/pokemon'
import { getLocale } from '@/utils/lang'
import { POKEMONS_OFFSET, POKEMONS_TOTAL, SITE_NAME } from '@/utils/constants'
import { Locale } from '@/types/lang'
import { env } from '@/env'

const List = (): JSX.Element => {
    const router = useRouter()
    const lang = useRecoilValue(langAtom)

    const [nbPokemons, setNbPokemons] = useState(POKEMONS_OFFSET)
    const [listPokemons, setListPokemons] = useState<string[]>([])

    const locale = getLocale(router.locale)

    // Add Pokémons
    const addPokemons = (): void => {
        const listTmp = listPokemons

        while (
            listTmp.length < nbPokemons + POKEMONS_OFFSET &&
            listTmp.length < POKEMONS_TOTAL
        ) {
            listTmp.push(PokeScan.getName(listTmp.length + 1, locale))
        }

        setNbPokemons(nbPokemons + 20)
        setListPokemons(listTmp)
    }

    // Add first Pokémons when visiting the page
    useEffect(() => {
        const listeTmp = []

        while (listeTmp.length < POKEMONS_OFFSET) {
            listeTmp.push(PokeScan.getName(listeTmp.length + 1, locale))
        }

        setListPokemons(listeTmp)
        setNbPokemons(20)
    }, [locale])

    return (
        <>
            <Head>
                <title>
                    {lang.pages.liste.titre} - {SITE_NAME}
                </title>

                <link
                    rel='alternate'
                    hrefLang={Locale.enum.en}
                    href={`${env.NEXT_PUBLIC_ADDRESS}/list`}
                />

                <link
                    rel='alternate'
                    hrefLang={Locale.enum.fr}
                    href={`${env.NEXT_PUBLIC_ADDRESS}/${Locale.enum.fr}/list`}
                />

                <link
                    rel='alternate'
                    hrefLang={Locale.enum.de}
                    href={`${env.NEXT_PUBLIC_ADDRESS}/${Locale.enum.de}/list`}
                />
            </Head>

            <>
                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    {/* Pokémon cards */}
                    <Box
                        sx={{
                            display: 'flex',
                            justifyContent: 'center',
                            flexWrap: 'wrap',
                            mx: {
                                xs: '2vw',
                                sm: '5vw',
                                md: '10vw',
                                lg: '15vw',
                                xl: '20vw',
                            },
                        }}
                    >
                        {listPokemons.map((name, index) => (
                            <PokemonCardInList
                                key={name}
                                name={name}
                                id={index + 1}
                            />
                        ))}
                    </Box>

                    {/* Button more */}
                    {nbPokemons < POKEMONS_TOTAL && (
                        <Button
                            onClick={addPokemons}
                            sx={{
                                mt: '20px',
                                color: 'text.primary',
                                bgcolor: 'primary.main',
                                border: '2px inset transparent',
                                '&:hover': {
                                    bgcolor: 'primary.light',
                                },
                                '&:focus': {
                                    bgcolor: 'primary.light',
                                    border: '2px solid',
                                    borderColor: 'primary.main',
                                },
                            }}
                        >
                            {lang.pages.liste.plus}
                        </Button>
                    )}
                </Box>
            </>
        </>
    )
}

export default List
