import { GetServerSideProps } from 'next'
import { getServerSideSitemapLegacy } from 'next-sitemap'
import PokeScan from 'pokemon'

import { urls } from '@/utils/constants'
import { Locale } from '@/types/lang'
import { env } from '@/env'

export const getServerSideProps: GetServerSideProps = async (ctx) => {
    const pokemonList = PokeScan.all()

    const urlsDE = [
        { loc: `${env.NEXT_PUBLIC_ADDRESS}/${Locale.enum.de}` },
        {
            loc: `${env.NEXT_PUBLIC_ADDRESS}/${Locale.enum.de}${urls.POKEMON_LIST}`,
        },
    ]

    for (const p of pokemonList) {
        urlsDE.push({
            loc: `${env.NEXT_PUBLIC_ADDRESS}/${Locale.enum.de}/${p}`,
        })
    }

    return getServerSideSitemapLegacy(ctx, urlsDE)
}

/* eslint-disable-next-line import/no-anonymous-default-export */
export default (): void => {
    /* Empty */
}
