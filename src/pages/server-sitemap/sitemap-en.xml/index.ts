import { GetServerSideProps } from 'next'
import { getServerSideSitemapLegacy } from 'next-sitemap'
import PokeScan from 'pokemon'

import { urls } from '@/utils/constants'
import { env } from '@/env'

export const getServerSideProps: GetServerSideProps = async (ctx) => {
    const pokemonList = PokeScan.all()

    const urlsEN = [
        { loc: env.NEXT_PUBLIC_ADDRESS },
        { loc: `${env.NEXT_PUBLIC_ADDRESS}${urls.POKEMON_LIST}` },
    ]

    for (const p of pokemonList) {
        urlsEN.push({ loc: `${env.NEXT_PUBLIC_ADDRESS}/${p}` })
    }

    return getServerSideSitemapLegacy(ctx, urlsEN)
}

/* eslint-disable-next-line import/no-anonymous-default-export */
export default (): void => {
    /* Empty */
}
