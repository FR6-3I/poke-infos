import { GetServerSideProps } from 'next'
import { getServerSideSitemapLegacy } from 'next-sitemap'
import PokeScan from 'pokemon'

import { urls } from '@/utils/constants'
import { Locale } from '@/types/lang'
import { env } from '@/env'

export const getServerSideProps: GetServerSideProps = async (ctx) => {
    const pokemonList = PokeScan.all()

    const urlsFR = [
        { loc: `${env.NEXT_PUBLIC_ADDRESS}/${Locale.enum.fr}` },
        {
            loc: `${env.NEXT_PUBLIC_ADDRESS}/${Locale.enum.fr}${urls.POKEMON_LIST}`,
        },
    ]

    for (const p of pokemonList) {
        urlsFR.push({
            loc: `${env.NEXT_PUBLIC_ADDRESS}/${Locale.enum.fr}/${p}`,
        })
    }

    return getServerSideSitemapLegacy(ctx, urlsFR)
}

/* eslint-disable-next-line import/no-anonymous-default-export */
export default (): void => {
    /* Empty */
}
