import { atom } from 'recoil'
import { Theme } from '@mui/material'

import classic from '@/themes/classic'
import en from '@/locales/en'
import { ThemeName } from '@/types/theme'

export const themeNameAtom = atom<ThemeName>({
    key: 'themeNameAtom',
    default: ThemeName.enum.CLASSIC,
})

export const themeValueAtom = atom<Theme>({
    key: 'themeValueAtom',
    default: classic,
})

export const langAtom = atom<typeof en>({
    key: 'langAtom',
    default: en,
})
