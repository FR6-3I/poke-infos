import { useRouter } from 'next/router'
import { useSetRecoilState } from 'recoil'
import { getCookie, setCookie } from 'typescript-cookie'

import { langAtom } from '@/recoil/atoms'
import fr from '@/locales/fr'
import de from '@/locales/de'
import en from '@/locales/en'
import { COOKIE_LOCALE, COOKIE_SETTINGS } from '@/utils/constants'
import { Locale } from '@/types/lang'

export const useLangStore = () => {
    const router = useRouter()
    const setLang = useSetRecoilState(langAtom)

    const { asPath, locale, pathname, query } = router

    /** Initialise the theme depending on router locale. */
    const initLang = (): void => {
        const localeCookieParsed = Locale.safeParse(getCookie(COOKIE_LOCALE))
        const localeParsed = Locale.safeParse(locale)

        if (!localeCookieParsed && !localeParsed.success) return

        if (
            localeCookieParsed.success &&
            localeCookieParsed.data === Locale.enum.fr
        )
            return setLang(fr)

        if (
            localeCookieParsed.success &&
            localeCookieParsed.data === Locale.enum.de
        )
            return setLang(de)

        if (localeParsed.success && localeParsed.data === Locale.enum.fr) {
            setLang(fr)
        }

        if (localeParsed.success && localeParsed.data === Locale.enum.de) {
            setLang(de)
        }
    }

    /** Set the new language. */
    const setNewLang = (newLocale: Locale): void => {
        if (newLocale === Locale.enum.fr) {
            setCookie(COOKIE_LOCALE, newLocale, COOKIE_SETTINGS)
            setLang(fr)

            router
                .push({ pathname, query }, asPath, { locale: newLocale })
                .catch(() => {
                    /* Empty */
                })

            return
        }

        if (newLocale === Locale.enum.de) {
            setCookie(COOKIE_LOCALE, newLocale, COOKIE_SETTINGS)
            setLang(de)

            router
                .push({ pathname, query }, asPath, { locale: newLocale })
                .catch(() => {
                    /* Empty */
                })

            return
        }

        setCookie(COOKIE_LOCALE, Locale.enum.en, COOKIE_SETTINGS)
        setLang(en)

        router
            .push({ pathname, query }, asPath, { locale: Locale.enum.en })
            .catch(() => {
                /* Empty */
            })
    }

    return {
        initLang,
        setNewLang,
    }
}
