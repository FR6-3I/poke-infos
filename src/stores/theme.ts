import { useSetRecoilState } from 'recoil'
import { getCookie, setCookie } from 'typescript-cookie'

import { themeNameAtom, themeValueAtom } from '@/recoil/atoms'
import gengar from '@/themes/gengar'
import pokeball from '@/themes/pokeball'
import classic from '@/themes/classic'
import { COOKIE_SETTINGS, COOKIE_THEME } from '@/utils/constants'
import { ThemeName } from '@/types/theme'

export const useThemeStore = () => {
    const setThemeName = useSetRecoilState(themeNameAtom)
    const setThemeValue = useSetRecoilState(themeValueAtom)

    /** Initialise the theme depending on cookie theme. */
    const initTheme = (): void => {
        const themeCookieParsed = ThemeName.safeParse(getCookie(COOKIE_THEME))

        if (!themeCookieParsed.success) return

        if (themeCookieParsed.data === ThemeName.enum.POKEBALL) {
            setThemeName(themeCookieParsed.data)
            setThemeValue(pokeball)
        }

        if (themeCookieParsed.data === ThemeName.enum.GENGAR) {
            setThemeName(themeCookieParsed.data)
            setThemeValue(gengar)
        }
    }

    /** Set the new theme. */
    const setTheme = (newTheme: ThemeName): void => {
        if (newTheme === ThemeName.enum.POKEBALL) {
            setCookie(COOKIE_THEME, newTheme, COOKIE_SETTINGS)
            setThemeName(newTheme)
            setThemeValue(pokeball)

            return
        }

        if (newTheme === ThemeName.enum.GENGAR) {
            setCookie(COOKIE_THEME, newTheme, COOKIE_SETTINGS)
            setThemeName(newTheme)
            setThemeValue(gengar)

            return
        }

        setCookie(COOKIE_THEME, ThemeName.enum.CLASSIC, COOKIE_SETTINGS)
        setThemeName(ThemeName.enum.CLASSIC)
        setThemeValue(classic)
    }

    return {
        initTheme,
        setTheme,
    }
}
