import { createTheme, responsiveFontSizes } from '@mui/material'
import { amber, blueGrey, grey, indigo } from '@mui/material/colors'

const classic = responsiveFontSizes(
    createTheme({
        palette: {
            primary: {
                main: indigo[500],
            },
            background: {
                default: amber[500],
                paper: amber[200],
            },
            text: {
                primary: grey[100],
                secondary: blueGrey[900],
            },
        },

        components: {
            MuiPaper: {
                styleOverrides: {
                    root: {
                        backgroundColor: indigo[500],
                    },
                },
            },
        },
    }),
)

export default classic
