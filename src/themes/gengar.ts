import { createTheme, responsiveFontSizes } from '@mui/material'
import { deepPurple, grey, purple } from '@mui/material/colors'

const gengar = responsiveFontSizes(
    createTheme({
        palette: {
            primary: {
                main: purple[800],
            },
            background: {
                default: deepPurple[900],
                paper: deepPurple[800],
            },
            text: {
                primary: grey[300],
                secondary: grey[400],
            },
        },

        components: {
            MuiPaper: {
                styleOverrides: {
                    root: {
                        backgroundColor: purple[800],
                    },
                },
            },
        },
    }),
)

export default gengar
