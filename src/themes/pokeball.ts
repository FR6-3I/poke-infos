import { createTheme, responsiveFontSizes } from '@mui/material'
import { grey, red } from '@mui/material/colors'

const pokeball = responsiveFontSizes(
    createTheme({
        palette: {
            primary: {
                main: red[700],
            },
            background: {
                default: grey[200],
                paper: grey[100],
            },
            text: {
                primary: grey[100],
                secondary: grey[800],
            },
        },

        components: {
            MuiPaper: {
                styleOverrides: {
                    root: {
                        backgroundColor: red[700],
                    },
                },
            },
        },
    }),
)

export default pokeball
