import { z } from 'zod'

export const Locale = z.enum(['de', 'en', 'fr'])
export type Locale = z.infer<typeof Locale>
