import { z } from 'zod'

// Name of the theme
export const ThemeName = z.enum(['CLASSIC', 'POKEBALL', 'GENGAR'])
export type ThemeName = z.infer<typeof ThemeName>
