import createCache, { EmotionCache } from '@emotion/cache'
import { CookieAttributes } from 'typescript-cookie/dist/types'

import { env } from '@/env'

export const SITE_NAME = 'Poké-Infos'
export const POKEMONS_TOTAL = 809
export const POKEMONS_OFFSET = 20

export const COOKIE_LOCALE = 'NEXT_LOCALE'
export const COOKIE_THEME = 'theme'
export const COOKIE_SETTINGS: CookieAttributes = {
    expires: 365 * 2,
    path: '/',
    sameSite: 'strict',
}

export const urls = {
    ERROR_404: '/404',
    HOMEPAGE: '/',
    POKEMON_LIST: '/list',
}

export const links = {
    PROFILE_GITLAB: 'https://gitlab.com/FR6-3I',
}

export const createEmotionCache = (): EmotionCache => {
    return createCache({ key: 'css', prepend: true })
}

/** Dynamic link for Pokémon image depending on ID. */
export const pokemonImageLink = (id: number): string => {
    if (id < 10) return `${env.NEXT_PUBLIC_API_IMAGE}/00${id}.png`
    if (id < 100) return `${env.NEXT_PUBLIC_API_IMAGE}/0${id}.png`

    return `${env.NEXT_PUBLIC_API_IMAGE}/${id}.png`
}
