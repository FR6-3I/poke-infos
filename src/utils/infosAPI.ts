/** Format the size to display. */
export const getSize = (size: number): string => {
    return `${(size / 10).toFixed(1).toLocaleString()} m`
}

/** Format the weight to display. */
export const getWeight = (weight: number): string => {
    return `${(weight / 10).toFixed(1).toLocaleString()} kg`
}

/** Format the type to display. */
export const getType = (type: string, lang: string): string => {
    switch (type) {
        case 'normal':
            return 'Normal'

        case 'fighting':
            switch (lang) {
                case 'fr':
                    return 'Combat'
                case 'de':
                    return 'Kampf'
                default:
                    return 'Fighting'
            }

        case 'flying':
            switch (lang) {
                case 'fr':
                    return 'Vol'
                case 'de':
                    return 'Flug'
                default:
                    return 'Flying'
            }

        case 'poison':
            switch (lang) {
                case 'de':
                    return 'Gift'
                default:
                    return 'Poison'
            }

        case 'ground':
            switch (lang) {
                case 'fr':
                    return 'Sol'
                case 'de':
                    return 'Boden'
                default:
                    return 'Ground'
            }

        case 'rock':
            switch (lang) {
                case 'fr':
                    return 'Roche'
                case 'de':
                    return 'Gestein'
                default:
                    return 'Rock'
            }

        case 'bug':
            switch (lang) {
                case 'fr':
                    return 'Insecte'
                case 'de':
                    return 'Käfer'
                default:
                    return 'Bug'
            }

        case 'ghost':
            switch (lang) {
                case 'fr':
                    return 'Spectre'
                case 'de':
                    return 'Geist'
                default:
                    return 'Ghost'
            }

        case 'steel':
            switch (lang) {
                case 'fr':
                    return 'Acier'
                case 'de':
                    return 'Stahl'
                default:
                    return 'Steel'
            }

        case 'fire':
            switch (lang) {
                case 'fr':
                    return 'Feu'
                case 'de':
                    return 'Feuer'
                default:
                    return 'Fire'
            }

        case 'water':
            switch (lang) {
                case 'fr':
                    return 'Eau'
                case 'de':
                    return 'Wasser'
                default:
                    return 'Water'
            }

        case 'grass':
            switch (lang) {
                case 'fr':
                    return 'Plante'
                case 'de':
                    return 'Pflanze'
                default:
                    return 'Grass'
            }

        case 'electric':
            switch (lang) {
                case 'fr':
                    return 'Électrik'
                case 'de':
                    return 'Elektro'
                default:
                    return 'Electric'
            }

        case 'psychic':
            switch (lang) {
                case 'fr':
                    return 'Psy'
                case 'de':
                    return 'Psycho'
                default:
                    return 'Psychic'
            }

        case 'ice':
            switch (lang) {
                case 'fr':
                    return 'Glace'
                case 'de':
                    return 'Eis'
                default:
                    return 'Ice'
            }

        case 'dragon':
            switch (lang) {
                case 'fr':
                    return 'Dragon'
                case 'de':
                    return 'Drache'
                default:
                    return 'Dragon'
            }

        case 'dark':
            switch (lang) {
                case 'fr':
                    return 'Ténèbres'
                case 'de':
                    return 'Unlicht'
                default:
                    return 'Dark'
            }

        case 'fairy':
            switch (lang) {
                case 'fr':
                    return 'Fée'
                case 'de':
                    return 'Fee'
                default:
                    return 'Fairy'
            }

        default:
            return '???'
    }
}
