import { Locale } from '@/types/lang'

/** Return the locale used by the application. */
export const getLocale = (locale: string | undefined): Locale => {
    const loc = Locale.safeParse(locale)
    const localeParsed = loc.success ? loc.data : Locale.enum.en

    return localeParsed
}

/** Name with the flag corresponding to the locale. */
export const langName: Record<Locale, string> = {
    en: '🇬🇧 English',
    fr: '🇫🇷 Français',
    de: '🇩🇪 Deutsch',
}
