import { Evolutions, IdEvolutions } from '@/pages/[pokemon]'

const setIdsEvol = (evolutions: Evolutions): IdEvolutions => {
    /** Get the ID at the end of the link. */
    const getIdLink = (lien: string): number => {
        const regex = lien.match(/\d/g)
        const resRegex = regex as unknown as number[]
        resRegex.shift()
        const res = Number(resRegex.join(''))

        return res
    }

    if (evolutions.chain.evolves_to[1]) {
        if (evolutions.chain.evolves_to[2]) {
            if (evolutions.chain.evolves_to[3]) {
                return {
                    premier: getIdLink(evolutions.chain.species.url),
                    second: {
                        premier2: getIdLink(
                            evolutions.chain.evolves_to[0].species.url,
                        ),
                        second2: getIdLink(
                            evolutions.chain.evolves_to[1].species.url,
                        ),
                        troisieme2: getIdLink(
                            evolutions.chain.evolves_to[2].species.url,
                        ),
                        quatrieme2: getIdLink(
                            evolutions.chain.evolves_to[3].species.url,
                        ),
                        cinquieme2: getIdLink(
                            evolutions.chain.evolves_to[4].species.url,
                        ),
                        sixieme2: getIdLink(
                            evolutions.chain.evolves_to[5].species.url,
                        ),
                        septieme2: getIdLink(
                            evolutions.chain.evolves_to[6].species.url,
                        ),
                        huitieme2: getIdLink(
                            evolutions.chain.evolves_to[7].species.url,
                        ),
                    },
                }
            }

            return {
                premier: getIdLink(evolutions.chain.species.url),
                second: {
                    premier2: getIdLink(
                        evolutions.chain.evolves_to[0].species.url,
                    ),
                    second2: getIdLink(
                        evolutions.chain.evolves_to[1].species.url,
                    ),
                    troisieme2: getIdLink(
                        evolutions.chain.evolves_to[2].species.url,
                    ),
                },
            }
        }

        if (
            evolutions.chain.evolves_to[0].evolves_to[0] &&
            evolutions.chain.evolves_to[1].evolves_to[0]
        ) {
            return {
                premier: getIdLink(evolutions.chain.species.url),
                second: {
                    premier2: getIdLink(
                        evolutions.chain.evolves_to[0].species.url,
                    ),
                    second2: getIdLink(
                        evolutions.chain.evolves_to[1].species.url,
                    ),
                },
                troisieme: {
                    premier3: getIdLink(
                        evolutions.chain.evolves_to[0].evolves_to[0].species
                            .url,
                    ),
                    second3: getIdLink(
                        evolutions.chain.evolves_to[1].evolves_to[0].species
                            .url,
                    ),
                },
            }
        }

        return {
            premier: getIdLink(evolutions.chain.species.url),
            second: {
                premier2: getIdLink(evolutions.chain.evolves_to[0].species.url),
                second2: getIdLink(evolutions.chain.evolves_to[1].species.url),
            },
        }
    }

    if (evolutions.chain.evolves_to[0].evolves_to[0]) {
        if (evolutions.chain.evolves_to[0].evolves_to[1]) {
            return {
                premier: getIdLink(evolutions.chain.species.url),
                second: {
                    premier2: getIdLink(
                        evolutions.chain.evolves_to[0].species.url,
                    ),
                },
                troisieme: {
                    premier3: getIdLink(
                        evolutions.chain.evolves_to[0].evolves_to[0].species
                            .url,
                    ),
                    second3: getIdLink(
                        evolutions.chain.evolves_to[0].evolves_to[1].species
                            .url,
                    ),
                },
            }
        }

        return {
            premier: getIdLink(evolutions.chain.species.url),
            second: {
                premier2: getIdLink(evolutions.chain.evolves_to[0].species.url),
            },
            troisieme: {
                premier3: getIdLink(
                    evolutions.chain.evolves_to[0].evolves_to[0].species.url,
                ),
            },
        }
    }

    return {
        premier: getIdLink(evolutions.chain.species.url),
        second: {
            premier2: getIdLink(evolutions.chain.evolves_to[0].species.url),
        },
    }
}

export default setIdsEvol
