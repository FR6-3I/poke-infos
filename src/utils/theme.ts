import fr from '@/locales/fr'
import { ThemeName } from '@/types/theme'

export const themeNameLang: Record<ThemeName, string> = {
    CLASSIC: fr.header.themes.classique,
    POKEBALL: fr.header.themes.pokeball,
    GENGAR: fr.header.themes.ectoplasma,
}
