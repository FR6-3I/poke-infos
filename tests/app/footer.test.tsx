import { render, screen } from '@testing-library/react'
import { RecoilRoot } from 'recoil'

import { Footer } from '@/components/app'
import lang from '@/locales/en'
import { links } from '@/utils/constants'

describe('footer', () => {
    it('links', () => {
        expect.assertions(1)

        render(
            <RecoilRoot>
                <Footer />
            </RecoilRoot>,
        )

        expect(screen.getByRole('link', { name: lang.footer })).toHaveAttribute(
            'href',
            links.PROFILE_GITLAB,
        )
    })
})
