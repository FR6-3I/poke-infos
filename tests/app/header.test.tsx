import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { RecoilRoot } from 'recoil'

import { Header } from '@/components/app'
import lang from '@/locales/en'
import { urls } from '@/utils/constants'
import mockNextRouter from '../mocks/router'

describe('header', () => {
    it('button homepage', () => {
        expect.assertions(1)

        mockNextRouter()

        render(
            <RecoilRoot>
                <Header />
            </RecoilRoot>,
        )

        expect(
            screen.getByRole('link', { name: lang.header.accueil }),
        ).toHaveAttribute('href', urls.HOMEPAGE)
    })

    it('button list', () => {
        expect.assertions(1)

        mockNextRouter()

        render(
            <RecoilRoot>
                <Header />
            </RecoilRoot>,
        )

        expect(
            screen.getByRole('link', { name: lang.header.liste }),
        ).toHaveAttribute('href', urls.POKEMON_LIST)
    })

    it('language selection', async () => {
        expect.assertions(4)

        const user = userEvent.setup()
        const mockRouter = mockNextRouter()

        render(
            <RecoilRoot>
                <Header />
            </RecoilRoot>,
        )

        const choiceLang = screen.getByRole('button', {
            name: lang.header.choixLangue,
        })

        await user.click(choiceLang)
        await user.click(screen.getByRole('menuitem', { name: 'English' }))

        await user.click(choiceLang)
        await user.click(screen.getByRole('menuitem', { name: 'Français' }))

        await user.click(choiceLang)
        await user.click(screen.getByRole('menuitem', { name: 'Deutsch' }))

        expect(mockRouter.push).toHaveBeenCalledTimes(3)

        expect(mockRouter.push).toHaveBeenCalledWith('/', '/', { locale: 'en' })
        expect(mockRouter.push).toHaveBeenCalledWith('/', '/', { locale: 'fr' })
        expect(mockRouter.push).toHaveBeenCalledWith('/', '/', { locale: 'de' })
    })

    it('theme selection', async () => {
        expect.assertions(4)

        const user = userEvent.setup()
        mockNextRouter()

        render(
            <RecoilRoot>
                <Header />
            </RecoilRoot>,
        )

        expect(screen.getByText('Classic')).toBeInTheDocument()

        await user.click(
            screen.getByRole('button', { name: lang.header.choixTheme }),
        )

        expect(
            screen.getByRole('menuitem', {
                name: lang.header.themes.classique,
            }),
        ).toBeInTheDocument()
        expect(
            screen.getByRole('menuitem', { name: lang.header.themes.pokeball }),
        ).toBeInTheDocument()
        expect(
            screen.getByRole('menuitem', {
                name: lang.header.themes.ectoplasma,
            }),
        ).toBeInTheDocument()
    })
})
