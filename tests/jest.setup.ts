import 'whatwg-fetch'

/* eslint-disable jest/no-hooks, jest/require-top-level-describe */
import { server } from '@/mocks'

beforeAll(() => server.listen({ onUnhandledRequest: 'bypass' }))

afterEach(() => server.resetHandlers())

afterAll(() => server.close())
