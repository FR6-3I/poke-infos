import { NextRouter } from 'next/router'
import * as nextRouter from 'next/router'

const router = jest.spyOn(nextRouter, 'useRouter')

const createMockRouter = (overrides: Partial<NextRouter>) => {
    return {
        basePath: '',
        pathname: '/',
        route: '/',
        query: {},
        asPath: '/',
        back: jest.fn(() => Promise.resolve(true)),
        beforePopState: jest.fn(() => Promise.resolve(true)),
        prefetch: jest.fn(() => Promise.resolve()),
        push: jest.fn(() => Promise.resolve(true)),
        reload: jest.fn(() => Promise.resolve(true)),
        replace: jest.fn(() => Promise.resolve(true)),
        events: {
            on: jest.fn(() => Promise.resolve(true)),
            off: jest.fn(() => Promise.resolve(true)),
            emit: jest.fn(() => Promise.resolve(true)),
        },
        isFallback: false,
        isLocaleDomain: false,
        isReady: true,
        defaultLocale: 'en',
        isPreview: false,
        forward: jest.fn(() => Promise.resolve(true)),
        ...overrides,
    }
}

/**
 * Mock the `useRouter()` hook and return the mocked router instance.
 */
const mockNextRouter = (overrides: Partial<NextRouter> = {}) => {
    const mockRouterObject = createMockRouter(overrides)

    router.mockImplementation(() => mockRouterObject)

    return mockRouterObject
}

export default mockNextRouter
