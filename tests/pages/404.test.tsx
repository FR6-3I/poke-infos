import { render, screen } from '@testing-library/react'
import { RecoilRoot } from 'recoil'

import Error from '@/pages/404'
import lang from '@/locales/en'

describe('page error 404', () => {
    it('general', async () => {
        expect.assertions(5)

        render(
            <RecoilRoot>
                <Error />
            </RecoilRoot>,
        )

        await expect(
            screen.findAllByRole('img', { name: 'O' }),
        ).resolves.toBeArrayOfSize(2)
        expect(screen.getByRole('img', { name: 'P' })).toBeInTheDocument()
        expect(screen.getByRole('img', { name: 'S' })).toBeInTheDocument()
        expect(screen.getByRole('img', { name: '!' })).toBeInTheDocument()
        expect(
            screen.getByRole('heading', {
                level: 1,
                name: lang.pages.erreur.message,
            }),
        ).toBeInTheDocument()
    })
})
