import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { RecoilRoot } from 'recoil'
import PokeScan from 'pokemon'

import Homepage from '@/pages/index'
import lang from '@/locales/en'
import mockNextRouter from '../mocks/router'

interface MockedRouterCall {
    mock: {
        calls: {
            query: {
                pokemon: string
            }
        }[][]
    }
}

describe('page homepage', () => {
    it('format input', async () => {
        expect.assertions(1)

        const user = userEvent.setup()
        mockNextRouter()

        render(
            <RecoilRoot>
                <Homepage />
            </RecoilRoot>,
        )

        const field = screen.getByRole('textbox', {
            name: lang.pages.index.label,
        })

        await user.type(field, 'évOlI')

        expect(field).toHaveValue('Évoli')
    })

    it('button search', async () => {
        expect.assertions(2)

        const user = userEvent.setup()
        const mockRouter = mockNextRouter()

        render(
            <RecoilRoot>
                <Homepage />
            </RecoilRoot>,
        )

        const field = screen.getByRole('textbox', {
            name: lang.pages.index.label,
        })

        await user.click(
            screen.getByRole('button', { name: lang.pages.index.pokemon }),
        )
        await user.type(field, '{enter}')
        await user.type(field, '76{enter}')

        expect(mockRouter.push).toHaveBeenCalledTimes(1)
        expect(mockRouter.push).toHaveBeenCalledWith({
            pathname: '/[pokemon]',
            query: { pokemon: 'Golem' },
        })
    })

    it('button random', async () => {
        expect.assertions(2)

        const user = userEvent.setup()
        const mockRouter = mockNextRouter()

        render(
            <RecoilRoot>
                <Homepage />
            </RecoilRoot>,
        )

        await user.click(
            screen.getByRole('button', {
                name: lang.pages.index.pokemonAleatoire,
            }),
        )

        expect(mockRouter.push).toHaveBeenCalledTimes(1)

        const routerCall = mockRouter.push as unknown as MockedRouterCall
        const pokemonName = routerCall.mock.calls[0][0].query.pokemon
        const pokemonId = PokeScan.getId(pokemonName)

        expect(pokemonId).toBeNumber()
    })

    it('exception Nidoran', async () => {
        expect.assertions(4)

        const user = userEvent.setup()
        const mockRouter = mockNextRouter()

        render(
            <RecoilRoot>
                <Homepage />
            </RecoilRoot>,
        )

        await user.type(
            screen.getByRole('textbox', { name: lang.pages.index.label }),
            'nidoran{enter}',
        )

        expect(
            screen.getByRole('dialog', { name: lang.pages.index.nidoranTitre }),
        ).toBeInTheDocument()

        await user.click(
            screen.getByRole('button', { name: lang.pages.index.male }),
        )
        await user.click(
            screen.getByRole('button', { name: lang.pages.index.femelle }),
        )

        expect(mockRouter.push).toHaveBeenCalledTimes(2)

        expect(mockRouter.push).toHaveBeenCalledWith({
            pathname: '/[pokemon]',
            query: { pokemon: 'Nidoran♂' },
        })

        expect(mockRouter.push).toHaveBeenCalledWith({
            pathname: '/[pokemon]',
            query: { pokemon: 'Nidoran♀' },
        })
    })

    it('description', () => {
        expect.assertions(2)

        mockNextRouter()

        render(
            <RecoilRoot>
                <Homepage />
            </RecoilRoot>,
        )

        expect(
            screen.getByRole('heading', {
                level: 1,
                name: lang.pages.index.titre,
            }),
        ).toBeInTheDocument()

        expect(screen.getByText(lang.pages.index.legende)).toBeInTheDocument()
    })

    it('not found', async () => {
        expect.assertions(2)

        const user = userEvent.setup()
        const mockRouter = mockNextRouter()

        render(
            <RecoilRoot>
                <Homepage />
            </RecoilRoot>,
        )

        await user.type(
            screen.getByRole('textbox', { name: lang.pages.index.label }),
            'sdfsdf{enter}',
        )

        expect(
            screen.getByRole('alert', { name: lang.pages.index.alerte }),
        ).toBeInTheDocument()
        expect(mockRouter.push).toHaveBeenCalledTimes(0)
    })
})
