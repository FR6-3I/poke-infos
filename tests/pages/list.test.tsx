import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { RecoilRoot } from 'recoil'

import List from '@/pages/list'
import lang from '@/locales/en'
import { POKEMONS_OFFSET, POKEMONS_TOTAL } from '@/utils/constants'
import mockNextRouter from '../mocks/router'

describe('page Pokémon list', () => {
    it('general', async () => {
        expect.assertions(5)

        const user = userEvent.setup()
        mockNextRouter()

        render(
            <RecoilRoot>
                <List />
            </RecoilRoot>,
        )
        const plus = screen.getByRole('button', { name: lang.pages.liste.plus })

        expect(screen.getAllByRole('link', { name: /n°/ })).toBeArrayOfSize(
            POKEMONS_OFFSET,
        )

        const maxClick = Math.ceil(
            (POKEMONS_TOTAL - POKEMONS_OFFSET) / POKEMONS_OFFSET,
        )

        for (let i = 0; i < maxClick; i++) {
            await user.click(plus)
        }

        // ByRole replaced with ByText to prevent infinite loop
        expect(screen.getAllByText(/n°/)).toBeArrayOfSize(POKEMONS_TOTAL)
        expect(screen.getByText('Blacephalon')).toBeInTheDocument()
        expect(screen.getByText('Melmetal')).toBeInTheDocument()
        expect(
            screen.queryByText(lang.pages.liste.plus),
        ).not.toBeInTheDocument()
    }, 40_000)
})
