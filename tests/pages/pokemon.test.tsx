import { render, screen, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { RecoilRoot } from 'recoil'

import Pokemon from '@/pages/[pokemon]'
import lang from '@/locales/en'
import { urls } from '@/utils/constants'
import mockNextRouter from '../mocks/router'

describe('page Pokémon data', () => {
    it('bad link redirection', () => {
        expect.assertions(2)

        const mockRouter = mockNextRouter({
            query: { pokemon: 'sdfsdf' },
        })

        render(
            <RecoilRoot>
                <Pokemon />
            </RecoilRoot>,
        )

        expect(mockRouter.replace).toHaveBeenCalledTimes(1)
        expect(mockRouter.replace).toHaveBeenCalledWith(urls.ERROR_404)
    })

    it('arrow previous', async () => {
        expect.assertions(3)

        const user = userEvent.setup()
        const mockRouter = mockNextRouter({
            query: { pokemon: 'Melmetal' },
        })

        render(
            <RecoilRoot>
                <Pokemon />
            </RecoilRoot>,
        )

        await user.click(
            await screen.findByRole('button', {
                name: lang.pages.pokemon.precedent,
            }),
        )

        await waitFor(() =>
            expect(
                screen.queryByRole('button', {
                    name: lang.pages.pokemon.suivant,
                }),
            ).not.toBeInTheDocument(),
        )

        expect(mockRouter.push).toHaveBeenCalledTimes(1)
        expect(mockRouter.push).toHaveBeenCalledWith({
            pathname: '/[pokemon]',
            query: { pokemon: 'Meltan' },
        })
    })

    it('arrow next', async () => {
        expect.assertions(3)

        const user = userEvent.setup()
        const mockRouter = mockNextRouter({
            query: { pokemon: 'Bulbasaur' },
        })

        render(
            <RecoilRoot>
                <Pokemon />
            </RecoilRoot>,
        )

        await waitFor(() =>
            expect(
                screen.queryByRole('button', {
                    name: lang.pages.pokemon.precedent,
                }),
            ).not.toBeInTheDocument(),
        )

        await user.click(
            await screen.findByRole('button', {
                name: lang.pages.pokemon.suivant,
            }),
        )

        expect(mockRouter.push).toHaveBeenCalledTimes(1)
        expect(mockRouter.push).toHaveBeenCalledWith({
            pathname: '/[pokemon]',
            query: { pokemon: 'Ivysaur' },
        })
    })

    it('card Pokémon', async () => {
        expect.assertions(5)

        mockNextRouter({
            query: { pokemon: 'Golem' },
        })

        render(
            <RecoilRoot>
                <Pokemon />
            </RecoilRoot>,
        )

        await expect(
            screen.findByRole('heading', { level: 1, name: 'Golem (n°76)' }),
        ).resolves.toBeInTheDocument()
        await expect(
            screen.findByText('Height: 1.4 m'),
        ).resolves.toBeInTheDocument()
        await expect(
            screen.findByText('Weight: 300.0 kg'),
        ).resolves.toBeInTheDocument()
        await expect(screen.findByText('Rock')).resolves.toBeInTheDocument()
        await expect(screen.findByText('Ground')).resolves.toBeInTheDocument()
    })

    it('card evolutions', async () => {
        expect.assertions(5)

        const user = userEvent.setup()
        const mockRouter = mockNextRouter({
            query: { pokemon: 'Golem' },
        })

        render(
            <RecoilRoot>
                <Pokemon />
            </RecoilRoot>,
        )

        await expect(
            screen.findByRole('heading', { level: 1, name: 'Evolutions' }),
        ).resolves.toBeInTheDocument()

        await user.click(
            await screen.findByRole('button', { name: 'Geodude (n°74)' }),
        )
        await user.click(
            await screen.findByRole('button', { name: 'Graveler (n°75)' }),
        )
        await user.click(await screen.findByText('Golem'))

        await expect(screen.findByText('(n°76)')).resolves.toBeInTheDocument()

        expect(mockRouter.push).toHaveBeenCalledTimes(2)

        expect(mockRouter.push).toHaveBeenCalledWith({
            pathname: '/[pokemon]',
            query: { pokemon: 'Geodude' },
        })

        expect(mockRouter.push).toHaveBeenCalledWith({
            pathname: '/[pokemon]',
            query: { pokemon: 'Graveler' },
        })
    })
})
